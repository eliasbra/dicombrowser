/*
 * $Id: AttributeAddDialog.java,v 1.1 2007/08/22 18:19:10 karchie Exp $
 * Copyright (c) 2007 Washington University
 */

package org.nrg.dcm.browse;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.util.TagUtils;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 */
@Slf4j
final class AttributeAddDialog extends JDialog implements ActionListener, DocumentListener {
    /**
     * @param owner Owning frame for this dialog.
     * @param table The table to populate the dialog with.
     */
    AttributeAddDialog(final Frame owner, final FileSetTableModel table) {
        super(owner, TITLE, true);
        setLocationRelativeTo(owner);
        initialize();
        _table = table;
    }

    public void changedUpdate(final DocumentEvent event) {
        _attrDescLabel.setText("");
        try {
            final int tag = parseTag();
            _attrDescLabel.setText(getTagDescription(tag));
            _addButton.setEnabled(true);
        } catch (NumberFormatException ex) {
            _addButton.setEnabled(false);
        }
    }

    public void insertUpdate(final DocumentEvent event) {
        changedUpdate(event);
    }

    public void removeUpdate(final DocumentEvent event) {
        changedUpdate(event);
    }

    public void actionPerformed(final ActionEvent event) {
        final String command = event.getActionCommand();

        if (CANCEL_BUTTON.equals(command)) {
            // drop this operation and move on
            log.trace("User clicked the cancel button, terminating.");
        } else if (ADD_BUTTON.equals(command) || event.getSource().equals(getValueField())) {
            try {
                _table.doOperation(OperationFactory.getInstance(OperationFactory.Add, parseTag(), getValueField().getText()), -1);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, String.format("(%s,%s) is not a valid DICOM tag", getTagHighField().getText(), getTagLowField().getText()));
                return;
            }
        } else {
            throw new RuntimeException("Unimplemented action: " + command);
        }
        setVisible(false);    // Either button means we're done with this dialog.
    }

    private int parseTag() {
        return (Integer.parseInt(getTagHighField().getText(), 16) << 16) + Integer.parseInt(getTagLowField().getText(), 16);
    }

    /**
     * This method initializes this
     */
    private void initialize() {
        setSize(260, 168);
        setContentPane(getJContentPane());
    }

    /**
     * This method initializes _contentPane
     *
     * @return javax.swing.JPanel
     */
    private JPanel getJContentPane() {
        if (_contentPane == null) {
            _contentPane = new JPanel(new GridBagLayout());

            final GridBagConstraints tagPC = new GridBagConstraints();
            tagPC.gridy = 0;
            _contentPane.add(getTagPanel(), tagPC);

            _attrDescLabel = new JLabel("");
            _attrDescLabel.setPreferredSize(new Dimension(240, 20));
            final GridBagConstraints descLC = new GridBagConstraints();
            descLC.gridy = 1;
            _contentPane.add(_attrDescLabel, descLC);

            final GridBagConstraints valuePC = new GridBagConstraints();
            valuePC.gridy = 2;
            _contentPane.add(getValuePanel(), valuePC);

            final GridBagConstraints actionPC = new GridBagConstraints();
            actionPC.gridy = 3;
            actionPC.anchor = GridBagConstraints.LINE_END;
            _contentPane.add(getActionPanel(), actionPC);
        }

        return _contentPane;
    }

    /**
     * This method initializes tagPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getTagPanel() {
        if (_tagPanel == null) {
            _tagPanel = new JPanel();

            final JLabel             tagLabel = new JLabel("New attribute tag:");
            final GridBagConstraints tagLC    = new GridBagConstraints();
            tagLC.insets = new Insets(0, 0, 0, 5);
            tagLC.gridx = 0;
            _tagPanel.add(tagLabel);

            final JLabel             tagOPLabel = new JLabel("(");
            final GridBagConstraints tagOPLC    = new GridBagConstraints();
            tagOPLC.gridx = 1;
            _tagPanel.add(tagOPLabel);

            final GridBagConstraints tagHFC = new GridBagConstraints();
            tagHFC.gridx = 2;
            _tagPanel.add(getTagHighField());

            final JLabel             tagCommaLabel = new JLabel(",");
            final GridBagConstraints tagCommaC     = new GridBagConstraints();
            tagCommaC.gridx = 3;
            _tagPanel.add(tagCommaLabel);

            final GridBagConstraints tagLFC = new GridBagConstraints();
            tagLFC.gridx = 4;
            _tagPanel.add(getTagLowField());

            final JLabel             tagCPLabel = new JLabel(")");
            final GridBagConstraints tagCPLC    = new GridBagConstraints();
            tagCPLC.gridx = 5;
            _tagPanel.add(tagCPLabel);
        }
        return _tagPanel;
    }

    /**
     * This method initializes valuePanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getValuePanel() {
        if (_valuePanel == null) {
            _valuePanel = new JPanel();
            _valuePanel.add(new JLabel("Value:"));
            _valuePanel.add(getValueField());
        }
        return _valuePanel;
    }

    /**
     * This method initializes tagHighField
     *
     * @return javax.swing.JTextField
     */
    private JTextField getTagHighField() {
        if (_tagHighField == null) {
            _tagHighField = getPopulatedTagField();
        }
        return _tagHighField;
    }

    /**
     * This method initializes tagLowField
     *
     * @return javax.swing.JTextField
     */
    private JTextField getTagLowField() {
        if (_tagLowField == null) {
            _tagLowField = getPopulatedTagField();
        }
        return _tagLowField;
    }

    private JTextField getPopulatedTagField() {
        final JTextField tagField = new JTextField();
        tagField.setPreferredSize(new Dimension(36, 20));
        tagField.getDocument().addDocumentListener(this);
        return tagField;
    }

    /**
     * This method initializes actionPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getActionPanel() {
        if (_actionPanel == null) {
            _actionPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            _actionPanel.add(getCancelButton(), null);
            _actionPanel.add(getAddButton(), null);
        }
        return _actionPanel;
    }

    /**
     * This method initializes valueField
     *
     * @return javax.swing.JTextField
     */
    private JTextField getValueField() {
        if (_valueField == null) {
            _valueField = new JTextField();
            _valueField.setPreferredSize(new Dimension(180, 20));
            _valueField.addActionListener(this);    // VK_ENTER in this field treated same as clicking "Add"
        }
        return _valueField;
    }

    /**
     * This method initializes cancelButton
     *
     * @return javax.swing.JButton
     */
    private JButton getCancelButton() {
        if (_cancelButton == null) {
            _cancelButton = new JButton();
            _cancelButton.setText(CANCEL_BUTTON);
            _cancelButton.setToolTipText("Don't add this attribute");
            _cancelButton.addActionListener(this);
        }
        return _cancelButton;
    }

    /**
     * This method initializes _addButton
     *
     * @return javax.swing.JButton
     */
    private JButton getAddButton() {
        if (_addButton == null) {
            _addButton = new JButton();
            _addButton.setEnabled(false);
            _addButton.setText(ADD_BUTTON);
            _addButton.addActionListener(this);
        }
        return _addButton;
    }

    private static String getTagDescription(final int tag) {
        final String description = TagUtils.toString(tag) + ": " + DICOM_REFERENCE.nameOf(tag);
        return StringUtils.equals("?", description) ? UNKNOWN : description;
    }

    private static final String      TITLE           = "Add new attribute";    // TODO: localize  //  @jve:decl-index=0:
    private static final String      CANCEL_BUTTON   = "Cancel";        // TODO: localize
    private static final String      ADD_BUTTON      = "Add";        // TODO: localize
    private static final String      UNKNOWN         = "Unknown attribute";    // TODO: localize
    private static final DicomObject DICOM_REFERENCE = new BasicDicomObject();    // used for name translation  //  @jve:decl-index=0:

    private final FileSetTableModel _table;

    private JPanel     _contentPane   = null;
    private JPanel     _tagPanel      = null;
    private JPanel     _valuePanel    = null;
    private JPanel     _actionPanel   = null;
    private JTextField _tagHighField  = null;
    private JTextField _tagLowField   = null;
    private JTextField _valueField    = null;
    private JLabel     _attrDescLabel = null;
    private JButton    _cancelButton  = null;
    private JButton    _addButton     = null;
}
