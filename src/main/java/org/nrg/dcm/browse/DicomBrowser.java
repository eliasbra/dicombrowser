/*
 * Copyright (c) 2006-2012 Washington University
 */

package org.nrg.dcm.browse;

import lombok.extern.slf4j.Slf4j;
import org.nrg.dcm.db.FileSet;
import org.nrg.dcm.edit.ScriptApplicator;
import org.nrg.dcm.edit.SessionVariablePanel;
import org.nrg.dcm.tags.PrivateTagChecker;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.TableColumn;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
@Slf4j
public class DicomBrowser extends JPanel implements ActionListener, ComponentListener, ListSelectionListener, TreeSelectionListener {
    static final String OPEN_DIR_PREF   = "choose.dir";
    static final String SCRIPT_DIR_PREF = "script.dir";

    public DicomBrowser(final JFrame frame, final FileSet fs) {
        super(new BorderLayout());
        this.frame = frame;

        treeModel = new FileSetTreeModel(frame, fs);
        tableModel = new FileSetTableModel(this, fs, EXECUTOR);

        tree = new JTree(treeModel);
        tree.setRootVisible(false);
        tree.setShowsRootHandles(true);
        tree.addTreeSelectionListener(this);
        tree.addTreeSelectionListener(tableModel);

        final JScrollPane treeView = new JScrollPane(tree);
        treeView.setMinimumSize(new Dimension(200, 200));
        treeView.setPreferredSize(new Dimension(200, 200));


        // Set up Actions: these can be invoked from the menu bar or from a popup.
        keepAction = new CommandAction(RESOURCES.getString(KEEP_ITEM), KeyEvent.VK_K);
        keepAction.setEnabled(false);
        needsAttrSelection.add(keepAction);

        clearAction = new CommandAction(RESOURCES.getString(CLEAR_ITEM), KeyEvent.VK_C);
        clearAction.setEnabled(false);  // has custom selection management

        deleteAction = new CommandAction(RESOURCES.getString(DELETE_ITEM), KeyEvent.VK_D);
        deleteAction.setEnabled(false);
        needsAttrSelection.add(deleteAction);

        addAction = new AddAction();
        addAction.setEnabled(false);
        needsFileSelection.add(addAction);

        viewAction = new ViewSlicesAction(VIEW_ITEM, fileSelection);
        viewAction.setEnabled(false);
        needsFileSelection.add(viewAction);

        closeFilesAction = new CloseFilesAction(treeModel, CLOSE_FILES_ITEM, fileSelection);
        closeFilesAction.setEnabled(false);
        needsFileSelection.add(closeFilesAction);

        undoAction = new UndoAction();
        undoAction.setEnabled(false);

        redoAction = new RedoAction();
        redoAction.setEnabled(false);

        // Set up the tree popup menu
        final JPopupMenu treePopup = new JPopupMenu();
        treePopup.add(new JMenuItem(viewAction));
        treePopup.add(new JMenuItem(closeFilesAction));

        final MouseListener treePopupListener = new PopupListener(treePopup);
        tree.addMouseListener(treePopupListener);

        table = new JTable(tableModel);
        table.getSelectionModel().addListSelectionListener(this);   // for menu enable/disable


        // Set up the table popup menu
        final JPopupMenu tablePopup = new JPopupMenu();
        tablePopup.add(new JMenuItem(undoAction));
        tablePopup.add(new JMenuItem(redoAction));
        tablePopup.addSeparator();
        tablePopup.add(new JMenuItem(keepAction));
        tablePopup.add(new JMenuItem(clearAction));
        tablePopup.add(new JMenuItem(deleteAction));
        tablePopup.add(new JMenuItem(addAction));

        final MouseListener tablePopupListener = new PopupListener(tablePopup);
        table.addMouseListener(tablePopupListener);


        // specialize editing of value column
        final TableColumn valueCol = table.getColumnModel().getColumn(FileSetTableModel.VALUE_COLUMN);
        cellEditor = new AttrTableCellEditor(frame, table);
        tree.addTreeSelectionListener(cellEditor);
        valueCol.setCellEditor(cellEditor);

        final JScrollPane tableView = new JScrollPane(table);
        table.setPreferredScrollableViewportSize(new Dimension(600, 600));
        for (int i = 0; i < COLUMN_WIDTHS.length; i++) {
            table.getColumnModel().getColumn(i).setPreferredWidth(COLUMN_WIDTHS[i]);
        }

        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setLeftComponent(treeView);
        splitPane.setRightComponent(tableView);
        add(splitPane, BorderLayout.CENTER);
        frame.addComponentListener(this);

        statusBar = new StatusBar();
        add(statusBar, BorderLayout.SOUTH);
    }

    public static void main(final String[] args) {
        // if DicomBrowser.value.maxlen is set, assign the maximum value length preference
        final String maxValueLen = System.getProperty("DicomBrowser." + MAX_LEN_PREF);
        if (null != maxValueLen) {
            PREFERENCES.putInt(MAX_LEN_PREF, Integer.parseInt(maxValueLen));
        }

        SwingUtilities.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception ignored) {
            } // no big deal, we'll use default instead.
        });

        new FileSetReader(null, Arrays.stream(args).map(File::new).collect(Collectors.toList())).run();
    }

    @SuppressWarnings("unused")
    public static String formatDicomTag(final int tag) {
        final long upper = (tag & MASK_UPPER) >>> 16;
        final long lower = tag & MASK_LOWER;
        return String.format(DICOM_TAG_FORMAT, upper, lower);
    }

    /**
     * Returns the Preferences for this application
     *
     * @return Preferences object
     */
    Preferences getPrefs() {
        return PREFERENCES;
    }

    JFrame getFrame() {
        return frame;
    }

    StatusBar getStatusBar() {
        return statusBar;
    }

    /**
     * Handles an event indicating the main frame has been hidden.
     *
     * @param event The hidden event object.
     */
    public void componentHidden(final ComponentEvent event) {
        log.debug("Got a component hidden event: {}", event);
    }

    /**
     * Handles an event indicating the main frame has been moved.
     *
     * @param event The moved event object.
     */
    public void componentMoved(final ComponentEvent event) {
        log.debug("Got a component moved event: {}", event);
    }

    /**
     * Handles an event indicating the main frame has been shown.
     *
     * @param event The shown event object.
     */
    public void componentShown(final ComponentEvent event) {
        log.debug("Got a component shown event: {}", event);
    }

    /**
     * Handles an event indicating the main frame has been resized. The contents should be resized accordingly.
     *
     * @param event The resize event object.
     */
    public void componentResized(final ComponentEvent event) {
        log.debug("Got a component resized event: {}", event);

        assert event.getComponent() == frame;
        final Dimension paned     = frame.getContentPane().getSize();
        final int       newHeight = paned.height - statusBar.getSize().height;
        splitPane.setSize(paned.width, newHeight);

        final Insets insets       = splitPane.getInsets();
        final int    borderHeight = insets.bottom + insets.top + 1;    // why 1? it works.
        for (int i = 0; i < splitPane.getComponentCount(); i++) {
            final Component c = splitPane.getComponent(i);
            final Dimension d = c.getSize();
            d.height = newHeight - borderHeight;
            c.setSize(d);
        }
    }

    /**
     * Handles an event indicating an action occurred.
     *
     * @param event The action event object.
     */
    public void actionPerformed(final ActionEvent event) {
        log.debug("Got an action performed event: {}", event);
        final String command = event.getActionCommand();
        if (command.equals(getString(OPEN_ITEM))) {
            openFiles(this);
        } else if (command.equals(getString(OPEN_NEW_WINDOW_ITEM))) {
            openFiles(null);
        } else if (command.equals(getString(SEND_ITEM))) {
            cellEditor.stopCellEditing();
            log.trace("creating CStoreDialog");
            final JDialog sendDialog = new CStoreDialog(this, tableModel);
            sendDialog.setVisible(true);
            log.trace("CStoreDialog {} created and made visible", sendDialog);
        } else if (command.equals(getString(SAVE_ITEM))) {
            cellEditor.stopCellEditing();
            final JDialog saveDialog = new SaveDialog(this, tableModel);
            saveDialog.setVisible(true);
        } else if (command.equals(getString(CLOSE_WIN_ITEM))) {
            closeBrowser();
        } else if (command.equals(getString(APPLY_SCRIPT_ITEM))) {
            cellEditor.stopCellEditing();
            applyScript();
        } else if (command.equals(getString(ABOUT_ITEM))) {
            final JDialog aboutDialog = new AboutDialog(frame, getString(ABOUT_TITLE));
            aboutDialog.setVisible(true);
        } else {
            throw new RuntimeException("Unimplemented operation: " + command);
        }
    }

    @SuppressWarnings("unused")
    public void doCommandOnSelection(final String name) {
        add(tableModel.addOperations(name, table.getSelectedRows()));
    }

    private static final String DICOM_TAG_FORMAT = "(%1$04X,%2$04X)";

    @SuppressWarnings("unused")
    void clearTreeSelection() {
        tree.getSelectionModel().clearSelection();
    }

    /**
     * Add a command to the Undo queue.  This also clears the Redo queue
     * (since no commands have been issued in this state).
     *
     * @param command Command to be enqueued
     */
    void add(final Command command) {
        undoable.push(command);
        undoAction.putValue(Action.NAME, UNDO_ITEM + " " + command.toString());
        undoAction.setEnabled(true);

        redoable.clear();
        redoAction.putValue(Action.NAME, REDO_ITEM);
        redoAction.setEnabled(false);
    }

    /**
     * Some menu items are enabled only if some attributes are selected
     */
    public void valueChanged(final ListSelectionEvent event) {
        final ListSelectionModel lsm      = (ListSelectionModel) event.getSource();
        final boolean            notEmpty = !lsm.isSelectionEmpty();
        for (final Action action : needsAttrSelection) {
            action.setEnabled(notEmpty);
        }
        clearAction.setEnabled(tableModel.allowClear(lsm));
    }

    public void valueChanged(final TreeSelectionEvent e) {
        final TreePath[] tps = e.getPaths();
        for (int i = 0; i < tps.length; i++) {
            if (e.isAddedPath(i)) {
                fileSelection.add(tps[i]);
            } else {
                fileSelection.remove(tps[i]);
            }
        }

        final boolean someSelected = !fileSelection.isEmpty();
        for (final Action action : needsFileSelection) {
            action.setEnabled(someSelected);
        }
    }

    private void add(final List<File> files) {
        assert !SwingUtilities.isEventDispatchThread();
        treeModel.add(files);
        if (treeModel.getChildCount(treeModel.getRoot()) > 0) {
            // now there are some files, so we can save or send them.
            SwingUtilities.invokeLater(() -> sendItem.setEnabled(true));
        }
    }

    /**
     * Allows the user to choose a script to apply to either
     * all files or just the selected files.
     */
    private void applyScript() {
        final JFileChooser fileChooser = new JFileChooser(PREFERENCES.get(SCRIPT_DIR_PREF, null));
        fileChooser.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(final File f) {
                return f.isDirectory() || f.getName().endsWith(SCRIPT_SUFFIX);
            }

            @Override
            public String getDescription() {
                return SCRIPT_FILTER_DESCRIPTION;
            }
        });
        fileChooser.setDialogTitle(getString(OPENING_SCRIPT));
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            PREFERENCES.put(SCRIPT_DIR_PREF, fileChooser.getCurrentDirectory().getPath());
            final ScriptApplicator applicator;
            try (final FileInputStream fin = new FileInputStream(fileChooser.getSelectedFile())) {
                applicator = new ScriptApplicator(fin);
            } catch (Throwable t) {
                JOptionPane.showMessageDialog(frame,
                                              String.format(getString(BAD_SCRIPT_MSG_FORMAT), t.getMessage()),
                                              getString(BAD_SCRIPT_TITLE),
                                              JOptionPane.ERROR_MESSAGE);
                return;
            }

            final String[] options = {getString(ALL_FILES_OPTION), getString(SELECTED_FILES_OPTION), getString(CANCEL_OPTION)};
            final int option = JOptionPane.showOptionDialog(frame,
                                                            getString(ALL_OR_SELECTED_FILES_QUESTION), getString(OPENING_SCRIPT),
                                                            JOptionPane.YES_NO_CANCEL_OPTION,
                                                            JOptionPane.QUESTION_MESSAGE, null,
                                                            options, options[2]);

            switch (option) {
                case 0:
                case 1:
                    if (SessionVariablePanel.withAssignedVariables(applicator.getSortedVariables(), tableModel.asMultimap())) {
                        final ProgressMonitor pm = new ProgressMonitor(this,
                                                                       RESOURCES.getString(APPLYING_SCRIPT),
                                                                       null, 0, 100);      // the thread will set the bounds.

                        // Generate a Command, apply it, and add it to the undo list.
                        final Runnable commandWorker = () -> {
                            Command command= null;
                            try {
                                command = tableModel.doScript(applicator.getStatements(), 1 == option, pm);
                            } catch (CloneNotSupportedException e) {
                                e.printStackTrace();
                            }
                            if (command != null) {
                                add(command);
                            }
                        };
                        new Thread(commandWorker).start();
                    }
                    break;

                case 2:
                default:
                    log.debug("No option or the default option were specified");
            }
        }
    }

    /**
     * From the indicated window, starts opening the named files into a new window.
     *
     * @param browser DicomBrowser instance.
     * @param files   List of files to be opened (if null or zero size, opens a Chooser)
     */
    private static void openFiles(final DicomBrowser browser, final File... files) {
        // need to be in Swing thread to use chooseFiles
        if (files == null || files.length == 0) {
            SwingUtilities.invokeLater(() -> {
                final JFrame frame  = browser == null ? null : browser.frame;
                final List<File> chosen = chooseFiles(frame);
                if (chosen != null && !chosen.isEmpty()) {
                    new Thread(new FileSetReader(browser, chosen)).start();
                }
            });
        } else {
            new Thread(new FileSetReader(browser, Arrays.asList(files))).start();
        }
    }

    private void closeBrowser() {
        frame.dispose();
        tableModel.dispose();
    }

    /**
     * Create a Frame to show one patient/dataset worth of data
     * This must be executed in the Swing thread
     */
    private static void createAndShowGUI(final FileSet fileSet) {
        final JFrame frame = new JFrame("DicomBrowser");
        InterfaceCounter.getInstance().register(frame);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setIconImage(new ImageIcon(ClassLoader.getSystemResource("icon.png")).getImage());

        final DicomBrowser browser = new DicomBrowser(frame, fileSet);
        browser.setOpaque(true);
        frame.setContentPane(browser);

        final JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        final JMenu fileMenu = new JMenu(RESOURCES.getString(FILE_MENU));
        fileMenu.setMnemonic(KeyEvent.VK_F);
        menuBar.add(fileMenu);

        final JMenuItem openItem = new JMenuItem(RESOURCES.getString(OPEN_ITEM));
        openItem.setMnemonic(KeyEvent.VK_O);
        openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        openItem.addActionListener(browser);
        fileMenu.add(openItem);

        final JMenuItem openNewWinItem = new JMenuItem(RESOURCES.getString(OPEN_NEW_WINDOW_ITEM));
        openNewWinItem.setMnemonic(KeyEvent.VK_N);
        openNewWinItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        openNewWinItem.addActionListener(browser);
        fileMenu.add(openNewWinItem);

        fileMenu.add(new JMenuItem(browser.closeFilesAction));

        fileMenu.addSeparator();

        browser.sendItem = new JMenuItem(RESOURCES.getString(SEND_ITEM));
        browser.sendItem.setMnemonic(KeyEvent.VK_E);
        browser.sendItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        browser.sendItem.addActionListener(browser);
        browser.sendItem.setEnabled(true);
        fileMenu.add(browser.sendItem);

        browser.saveItem = new JMenuItem(RESOURCES.getString(SAVE_ITEM));
        browser.saveItem.setMnemonic(KeyEvent.VK_S);
        browser.saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        browser.saveItem.addActionListener(browser);
        browser.saveItem.setEnabled(true);
        fileMenu.add(browser.saveItem);

        try {
            if (fileSet.isEmpty()) {
                browser.sendItem.setEnabled(false);
            }
        } catch (SQLException e) {
            browser.sendItem.setEnabled(false);
        }

        fileMenu.addSeparator();

        final JMenuItem closeItem = new JMenuItem(RESOURCES.getString(CLOSE_WIN_ITEM));
        closeItem.setMnemonic(KeyEvent.VK_C);
        closeItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        closeItem.addActionListener(browser);
        fileMenu.add(closeItem);

        final JMenu editMenu = new JMenu(RESOURCES.getString(EDIT_MENU));
        editMenu.setMnemonic(KeyEvent.VK_E);
        menuBar.add(editMenu);

        editMenu.add(new JMenuItem(browser.undoAction));
        editMenu.add(new JMenuItem(browser.redoAction));
        editMenu.addSeparator();
        editMenu.add(new JMenuItem(browser.keepAction));
        editMenu.add(new JMenuItem(browser.clearAction));
        editMenu.add(new JMenuItem(browser.deleteAction));
        editMenu.add(new JMenuItem(browser.addAction));
        editMenu.addSeparator();

        final JMenuItem applyScriptItem = new JMenuItem(RESOURCES.getString(APPLY_SCRIPT_ITEM));
        applyScriptItem.setMnemonic(KeyEvent.VK_A);
        applyScriptItem.addActionListener(browser);
        editMenu.add(applyScriptItem);

        final JMenu viewMenu = new JMenu(RESOURCES.getString(VIEW_MENU));
        menuBar.add(viewMenu);
        viewMenu.add(new JMenuItem(browser.viewAction));

        if (IS_MAC_OS) {
            new MacOSAppEventHandler(browser.frame, ABOUT_TITLE);
        } else {
            final JMenu helpMenu = new JMenu(RESOURCES.getString(HELP_MENU));
            helpMenu.setMnemonic(KeyEvent.VK_H);
            menuBar.add(helpMenu);

            final JMenuItem aboutItem = new JMenuItem(ABOUT_ITEM);
            aboutItem.setMnemonic(KeyEvent.VK_A);
            aboutItem.addActionListener(browser);
            helpMenu.add(aboutItem);
        }

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Open a Chooser dialog so the user can select files.
     * This must be executed in the Swing thread
     */
    private static List<File> chooseFiles(final Component parent) {
        final JFileChooser chooser = new JFileChooser(PREFERENCES.get(OPEN_DIR_PREF, null));
        chooser.setDialogTitle(SELECT_FILES);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        chooser.setMultiSelectionEnabled(true);
        if (chooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
            PREFERENCES.put(OPEN_DIR_PREF, chooser.getCurrentDirectory().getPath());
            return Arrays.asList(chooser.getSelectedFiles());
        } else {
            return null;
        }
    }

    /**
     * Moves a Command from one queue to another and makes appropriate menu changes.
     *
     * @param to         The destination queue
     * @param toAction   The destination action
     * @param toText     The destination description
     * @param from       The source queue
     * @param fromAction The source action
     * @param fromText   The source description
     *
     * @return The Command that was moved
     */
    private static Command moveCommand(final Stack<Command> to, final Action toAction, final String toText, Stack<Command> from, final Action fromAction, final String fromText) {
        assert !from.isEmpty();
        final Command command = from.pop();

        if (from.isEmpty()) {
            fromAction.putValue(Action.NAME, fromText);
            fromAction.setEnabled(false);
        } else {
            fromAction.putValue(Action.NAME, fromText + " " + from.peek().toString());
            assert fromAction.isEnabled();
        }

        to.push(command);

        toAction.putValue(Action.NAME, toText + " " + command.toString());
        toAction.setEnabled(true);

        return command;
    }

    /**
     * Returns the localized version of the indicated label
     *
     * @param label The resource label to retrieve.
     *
     * @return localized String
     */
    private static String getString(final String label) {
        return RESOURCES.getString(label);
    }

    private final class PopupListener extends MouseAdapter {
        private final JPopupMenu popup;

        PopupListener(final JPopupMenu popup) {
            super();
            this.popup = popup;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            checkPopup(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            checkPopup(e);
        }

        private void checkPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }

    private final class CommandAction extends AbstractAction {
        private static final long   serialVersionUID = 1L;
        private final        String name;

        CommandAction(final String name, final int mnemonic) {
            super(name);
            this.name = name;
            putValue(MNEMONIC_KEY, mnemonic);
        }

        public void actionPerformed(ActionEvent e) {
            boolean notAbbletoPerfomAction = false;
            if(name=="Clear"||name=="Keep") {
                cellEditor.cancelCellEditing();
            }
            if(name=="Delete") {
                cellEditor.cancelCellEditing();
                final StringBuilder message = new StringBuilder("Error: unable to delete row ");
                for(int row: table.getSelectedRows()) {
                    if(PrivateTagChecker.isNotDeletable(tableModel.getTagAtRow(row))) {
                        if(!notAbbletoPerfomAction) {
                            message.append(row+" "); 
                            notAbbletoPerfomAction = true;
                            continue;
                        }
                        message.append(","+row+" ");
                        
                    }
                }
                
                if(notAbbletoPerfomAction) {
                    message.append(" as it is Metainformation!");
                    JOptionPane.showMessageDialog(frame, message,
                            "Deletion failed",
                            JOptionPane.ERROR_MESSAGE); 
                }  
            }
            if(!notAbbletoPerfomAction) add(tableModel.addOperations(name, table.getSelectedRows()));
        }
    }

    private final class UndoAction extends AbstractAction {
        private static final long serialVersionUID = 1L;

        UndoAction() {
            super(RESOURCES.getString(UNDO_ITEM));
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        }

        public void actionPerformed(ActionEvent e) {
            final Command c = moveCommand(redoable, redoAction, RESOURCES.getString(REDO_ITEM),
                                          undoable, this, RESOURCES.getString(UNDO_ITEM));
            tableModel.undo(c);
        }
    }

    private final class RedoAction extends AbstractAction {
        private static final long serialVersionUID = 1L;

        RedoAction() {
            super(RESOURCES.getString(REDO_ITEM));
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        }

        public void actionPerformed(ActionEvent e) {
            final Command c = moveCommand(undoable, undoAction, RESOURCES.getString(UNDO_ITEM),
                                          redoable, this, RESOURCES.getString(REDO_ITEM));
            tableModel.redo(c);
        }
    }

    private final class AddAction extends AbstractAction {
        private static final long serialVersionUID = 1L;

        AddAction() {
            super(RESOURCES.getString(ADD_ITEM));
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_I, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        }

        public void actionPerformed(ActionEvent e) {
            cellEditor.cancelCellEditing();
            new AttributeAddDialog(frame, tableModel).setVisible(true);
        }
    }

    /**
     * Handles the slow work of loading a FileSet, then calls createAndShowGUI when ready.
     *
     * @author Kevin A. Archie <karchie@npg.wustl.edu>
     */
    private static final class FileSetReader implements Runnable {
        private final List<File>   files;
        private final DicomBrowser browser;

        FileSetReader(final DicomBrowser browser, final List<File> files) {
            this.files = files;
            this.browser = browser;
        }

        public void run() {
            if (browser == null) {
                final FileSet fileSet;
                try {
                    fileSet = new FileSet(files, true, SwingProgressMonitor.getMonitor(null, RESOURCES.getString(CHECKING_FILES), "", 0, 100));
                    fileSet.setMaxValueLength(PREFERENCES.getInt(MAX_LEN_PREF, DEFAULT_MAX_VALUE_LEN));
                    fileSet.setTruncateFormat(RESOURCES.getString(TRUNCATE_FORMAT));
                } catch (IOException | SQLException e) {
                    JOptionPane.showMessageDialog(null,
                                                  String.format(FILE_LOAD_FAILED_FORMAT, e.getMessage()),
                                                  FILE_LOAD_FAILED_TITLE,
                                                  JOptionPane.ERROR_MESSAGE);
                    return;
                }
                SwingUtilities.invokeLater(() -> createAndShowGUI(fileSet));
            } else {
                browser.add(files);
            }
        }
    }

    private static final ResourceBundle RESOURCES = new ListResourceBundle() {
        @Override
        public Object[][] getContents() {
            return contents;
        }

        private final Object[][] contents = {       // TODO: LOCALIZE THIS
                                                    {FILE_MENU, FILE_MENU},
                                                    {OPEN_ITEM, OPEN_ITEM},
                                                    {OPEN_NEW_WINDOW_ITEM, OPEN_NEW_WINDOW_ITEM},
                                                    {SEND_ITEM, SEND_ITEM},
                                                    {SAVE_ITEM, SAVE_ITEM},
                                                    {CLOSE_WIN_ITEM, CLOSE_WIN_ITEM},
                                                    {EDIT_MENU, EDIT_MENU},
                                                    {KEEP_ITEM, KEEP_ITEM},
                                                    {CLEAR_ITEM, CLEAR_ITEM},
                                                    {DELETE_ITEM, DELETE_ITEM},
                                                    {ADD_ITEM, ADD_ITEM},
                                                    {APPLY_SCRIPT_ITEM, APPLY_SCRIPT_ITEM},
                                                    {UNDO_ITEM, UNDO_ITEM},
                                                    {REDO_ITEM, REDO_ITEM},
                                                    {VIEW_MENU, VIEW_MENU},
                                                    {VIEW_ITEM, VIEW_ITEM},
                                                    {CLOSE_FILES_ITEM, CLOSE_FILES_ITEM},
                                                    {HELP_MENU, HELP_MENU},
                                                    {ABOUT_ITEM, ABOUT_ITEM},
                                                    {ABOUT_TITLE, ABOUT_TITLE},
                                                    {SELECT_FILES, SELECT_FILES},
                                                    {CHECKING_FILES, CHECKING_FILES},
                                                    {OPENING_SCRIPT, OPENING_SCRIPT},
                                                    {SCRIPT_FILTER_DESCRIPTION, SCRIPT_FILTER_DESCRIPTION},
                                                    {BAD_SCRIPT_TITLE, BAD_SCRIPT_TITLE},
                                                    {BAD_SCRIPT_MSG_FORMAT, BAD_SCRIPT_MSG_FORMAT},
                                                    {ALL_OR_SELECTED_FILES_QUESTION, ALL_OR_SELECTED_FILES_QUESTION},
                                                    {ALL_FILES_OPTION, ALL_FILES_OPTION},
                                                    {SELECTED_FILES_OPTION, SELECTED_FILES_OPTION},
                                                    {CANCEL_OPTION, CANCEL_OPTION},
                                                    {APPLYING_SCRIPT, APPLYING_SCRIPT},
                                                    {FILE_LOAD_FAILED_TITLE, FILE_LOAD_FAILED_TITLE},
                                                    {FILE_LOAD_FAILED_FORMAT, FILE_LOAD_FAILED_FORMAT},
                                                    {TRUNCATE_FORMAT, TRUNCATE_FORMAT}};
    };

    // maximum value length; 64 is the max length for UI, LO, PN, so
    // this covers most field types
    private static final int             DEFAULT_MAX_VALUE_LEN          = 64;
    private static final int[]           COLUMN_WIDTHS                  = {120, 160, 80, 380};
    private static final long            MASK_UPPER                     = 0xFFFF0000L;
    private static final long            MASK_LOWER                     = 0x0000FFFFL;
    private static final boolean         IS_MAC_OS                      = System.getProperty("mrj.version") != null;
    private static final String          FILE_MENU                      = "File";
    private static final String          OPEN_ITEM                      = "Open...";
    private static final String          OPEN_NEW_WINDOW_ITEM           = "Open in new window...";
    private static final String          SEND_ITEM                      = "Send...";
    private static final String          SAVE_ITEM                      = "Save...";
    private static final String          CLOSE_WIN_ITEM                 = "Close window";
    private static final String          UNDO_ITEM                      = "Undo";
    private static final String          REDO_ITEM                      = "Redo";
    private static final String          EDIT_MENU                      = "Edit";
    private static final String          KEEP_ITEM                      = "Keep";
    private static final String          CLEAR_ITEM                     = "Clear";
    private static final String          DELETE_ITEM                    = "Delete";
    private static final String          ADD_ITEM                       = "Add new attribute...";
    private static final String          APPLY_SCRIPT_ITEM              = "Apply script...";
    private static final String          VIEW_MENU                      = "View";
    private static final String          VIEW_ITEM                      = "View selected images";
    private static final String          CLOSE_FILES_ITEM               = "Close selected files";
    private static final String          HELP_MENU                      = "Help";
    private static final String          ABOUT_ITEM                     = "About DicomBrowser...";
    private static final String          ABOUT_TITLE                    = "DicomBrowser";
    private static final String          SELECT_FILES                   = "Select DICOM files";
    private static final String          CHECKING_FILES                 = "Checking files...";
    private static final String          OPENING_SCRIPT                 = "Opening metadata modification script...";
    private static final String          SCRIPT_FILTER_DESCRIPTION      = "DICOM metadata modification script (.das)";
    private static final String          BAD_SCRIPT_TITLE               = "Error in script";
    private static final String          BAD_SCRIPT_MSG_FORMAT          = "Error in script: %1$s";
    private static final String          ALL_OR_SELECTED_FILES_QUESTION = "Apply script to selected files only or to all files?";
    private static final String          ALL_FILES_OPTION               = "All files";
    private static final String          SELECTED_FILES_OPTION          = "Selected files only";
    private static final String          CANCEL_OPTION                  = "Cancel";
    private static final String          APPLYING_SCRIPT                = "Applying script...";
    private static final String          FILE_LOAD_FAILED_TITLE         = "File load failed";
    private static final String          FILE_LOAD_FAILED_FORMAT        = "Unable to read file: %1$s";
    private static final String          TRUNCATE_FORMAT                = "%1$s...(truncated)";
    private static final String          SCRIPT_SUFFIX                  = ".das";
    private static final String          MAX_LEN_PREF                   = "value.maxlen";
    private static final Preferences     PREFERENCES                    = Preferences.userNodeForPackage(DicomBrowser.class);
    private static final ExecutorService EXECUTOR                       = Executors.newCachedThreadPool();

    private final Collection<Action>   needsAttrSelection = new LinkedHashSet<>();
    private final Collection<Action>   needsFileSelection = new LinkedHashSet<>();
    private final Stack<Command>       undoable           = new Stack<>();
    private final Stack<Command>       redoable           = new Stack<>();
    private final Collection<TreePath> fileSelection      = new HashSet<>();

    private final Action              keepAction;
    private final Action              clearAction;
    private final Action              deleteAction;
    private final Action              addAction;
    private final Action              undoAction;
    private final Action              redoAction;
    private final ViewSlicesAction    viewAction;
    private final CloseFilesAction    closeFilesAction;
    private final JFrame              frame;
    private final JSplitPane          splitPane;
    private final JTree               tree;
    private final FileSetTreeModel    treeModel;
    private final JTable              table;
    private final FileSetTableModel   tableModel;
    private final AttrTableCellEditor cellEditor;
    private final StatusBar           statusBar;

    private JMenuItem saveItem;
    private JMenuItem sendItem;
}