/*
 * Copyright (c) 2006-2012 Washington University
 */
package org.nrg.dcm.browse;

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.net.TransferCapability;
import org.dcm4che2.util.TagUtils;
import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.db.DirectoryRecord;
import org.nrg.dcm.db.FileSet;
import org.nrg.dcm.db.ProgressMonitorI;
import org.nrg.dcm.edit.*;
import org.nrg.dcm.io.*;
import org.nrg.util.EditProgressMonitor;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.tree.TreePath;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
@Slf4j
public class FileSetTableModel extends AbstractTableModel implements TreeSelectionListener, MultifileExporter {
    static final String AE_TITLE = "DicomBrowser";

    private static final List<String> COLUMN_NAMES          = Arrays.asList("tag", "name", "action", "value");
    private static final String       SENDING_FILES         = "Sending files...";
    private static final String       WRITING_FILES         = "Writing files...";
    static final         String       COPYING_FILES         = "Copying files...";
    private static final String       READING_VALS_FORMAT   = "Reading values...%1$s";
    private static final String       OUT_OF_MEMORY_TITLE   = "Out of memory";
    private static final String       OUT_OF_MEMORY_MESSAGE =
        "DicomBrowser doesn't have enough memory to do that.\n" +
        "You should probably exit the program, as it may behave\n" +
        "unpredictably.\n" +
        "To avoid running out of memory, don't select so many\n" +
        "files at once.  You could also increase the Java memory\n" +
        "limit, e.g., by running DicomBrowser with the flag\n" +
        "-Xmx128M (or -Xmx512M, or more).";

    static final         ResourceBundle RESOURCE_BUNDLE          = new ListResourceBundle() {
        @Override
        public Object[][] getContents() {
            return contents;
        }

        private final Object[][] contents = {       // TODO: LOCALIZE THIS
                                                    {"tag", "Tag"},
                                                    {"name", "Name"},
                                                    {"action", "Action"},
                                                    {"value", "Value"},
                                                    {"preparing-files", "Preparing files..."},
                                                    {"sending-files", "Sending files..."},
                                                    {"reading-values", "Reading values..."},
                                                    {READING_VALS_FORMAT, READING_VALS_FORMAT},
                                                    {"done", ""},
                                                    {SENDING_FILES, SENDING_FILES},
                                                    {WRITING_FILES, WRITING_FILES},
                                                    {COPYING_FILES, COPYING_FILES},
                                                    {"deleted", "(deleted)"},
                                                    {OUT_OF_MEMORY_TITLE, OUT_OF_MEMORY_TITLE},
                                                    {OUT_OF_MEMORY_MESSAGE, OUT_OF_MEMORY_MESSAGE},
                                                    };
    };
    private static final int            MAX_TAG                  = Tag.IconImageSequence - 1;
    private static final long           CONTENTS_SHOW_INTERVAL   = 1000;
    private static final long           CONTENTS_UPDATE_INTERVAL = 4000;       // ms, heuristic
    private static final int            nCols                    = 4;
    private static final int            ACTION_COLUMN            = 2;
    static final         int            VALUE_COLUMN             = 3;

    static final int    MAX_VALUES_SHOWN = 4;
    static final String MORE_VALUES      = "...";

    private final DicomBrowser                       _dicomBrowser;
    private final ExecutorService                    _executorService;
    private final FileSet                            _fileSet;
    private final Set<TreePath>                      _fileSelections = new LinkedHashSet<>();
    private final Set<File>                          selectedFiles   = new LinkedHashSet<>();
    private Map<Integer, Map<File, Operation>> _operations     = new LinkedHashMap<>();
    private Map<Integer, Integer> _operationsTags = new LinkedHashMap<>();
    private LinkedListMultimap<Integer, String> vals= LinkedListMultimap.create();;

    private StatusBar.TaskMonitor _cachingProgress = null;

    private final List<MultiValueAttribute> _contents = new ArrayList<>();

    private class CacheBuilder implements Runnable {
        private final boolean wipe;

        CacheBuilder(final boolean shouldWipeTableFirst) {
            super();
            wipe = shouldWipeTableFirst;
        }

        public void run() {
            cacheValues(wipe);
        }
    }

    FileSetTableModel(final DicomBrowser dicomBrowser, final FileSet fileSet, final ExecutorService executorService) {
        _fileSet = fileSet;
        _dicomBrowser = dicomBrowser;
        _executorService = executorService;
    }

    final SetMultimap<Integer, String> asMultimap() {
        synchronized (_contents) {
            final SetMultimap<Integer, String> m = LinkedHashMultimap.create();
            for (final MultiValueAttribute attribute : _contents) {
                m.putAll(attribute.getTag(), Arrays.asList(attribute.getValues()));
            }
            return m;
        }
    }

    void dispose() {
        _operations.clear();
        _operationsTags.clear();
        _fileSet.dispose();
    }

    private void collectReferencedFiles(final DirectoryRecord root, final Set<File> files) {
        final Queue<DirectoryRecord> records = new LinkedList<>();
        records.add(root);
        while (records.peek() != null) {
            final DirectoryRecord directoryRecord    = records.poll();
            final String          referencedFilePath = directoryRecord.getValue(Tag.ReferencedFileID);
            if (referencedFilePath != null) {
                files.add(new File(referencedFilePath));
            }
            records.addAll(directoryRecord.getLower());
        }
    }

    private void updateValues(final LinkedListMultimap<Integer, String> vals){
        int row = 0;
        final List<MultiValueAttribute> newContents = Lists.newArrayList();
        final DicomObject ob = _fileSet.getDcmObjects().get(0);
        for (final Integer tag : vals.keySet()) {
            if (tag != null) {
                DicomElement de = ob.get(tag);
                    ArrayList<MultiValueAttribute> values = this.getDicomTagValues(de, ob, tag, false, 0, "== Item ");
                    if (de!=null&&de.toString().contains("SQ")) {
                        for (MultiValueAttribute mv : values) {
                            if (_operations.containsKey(row)) {
                                List<String> valuesofMv = Arrays.asList(mv.getValues());
                                for (final Map.Entry<File, Operation> me : _operations.get(row).entrySet()) {
                                    if (me.getValue().getName() == "Delete") {
                                        newContents.add(MultiValueAttribute.Factory.create(_fileSet, mv.getTag(), true,
                                                valuesofMv, true, true));
                                    } else if (me.getValue().getName() == "Assign") {
                                        String newValue = me.getValue().toString().substring(32);
                                        List<String> newValueList = new ArrayList<String>();
                                        newValueList.add(mv.getNameString());
                                        newValueList.add(newValue);
                                        newContents.add(MultiValueAttribute.Factory.create(_fileSet, mv.getTag(), true,
                                                newValueList, true, false));
                                    } else if (me.getValue().getName() == "Clear") {
                                        String newValue = "";
                                        List<String> newValueList = new ArrayList<String>();
                                        newValueList.add(mv.getNameString());
                                        newValueList.add(newValue);
                                        newContents.add(MultiValueAttribute.Factory.create(_fileSet, mv.getTag(), true,
                                                new ArrayList<String>(newValueList), true, false));
                                    } else if (me.getValue().getName() == "Keep") {
                                        newContents.add(mv);
                                    }
                                    break;
                                }
                            } else {
                                newContents.add(mv);
                            }
                            row++;
                        }
                    } else {
                        if (_operations.containsKey(row)&&_operationsTags.get(row).equals(tag)) {
                            
                            List<String> valuesofMv = vals.get(tag);
                            for (final Map.Entry<File, Operation> me : _operations.get(row).entrySet()) {
                                if (me.getValue().getName() == "Delete") {
                                    newContents.add(MultiValueAttribute.Factory.create(_fileSet, tag, true, valuesofMv,
                                            false, true));
                                } else if (me.getValue().getName() == "Assign") {
                                    String newValue = me.getValue().toString().substring(32);
                                    List<String> newValueList = new ArrayList<String>();
                                    newValueList.add(newValue);
                                    newContents.add(MultiValueAttribute.Factory.create(_fileSet, tag, true,
                                            new ArrayList<String>(newValueList), false, false));
                                } else if (me.getValue().getName() == "Clear") {
                                    String newValue = "";
                                    List<String> newValueList = new ArrayList<String>();
                                    newValueList.add(newValue);
                                    newContents.add(MultiValueAttribute.Factory.create(_fileSet, tag, true,
                                            new ArrayList<String>(newValueList), false, false));
                                } else if (me.getValue().getName() == "Keep") {
                                    newContents.add(MultiValueAttribute.Factory.create(_fileSet, tag, true,
                                            new ArrayList<String>(vals.get(tag)), false, false));
                                }
                                else if (me.getValue().getName() == "Addition") {
                                    String newValue = me.getValue().toString().substring(34);
                                    List<String> newValueList = new ArrayList<String>();
                                    newValueList.add(newValue);
                                    newContents.add(MultiValueAttribute.Factory.create(_fileSet, tag, true,
                                            new ArrayList<String>(newValueList), false, false)); 
                                }
                                break;
                            }
                            row++;
                        } else {
                            boolean isEmpty = true;
                            for (String s : vals.get(tag)) {
                                if (s != null) {
                                    isEmpty = false;
                                    break;
                                }
                            }
                            if (isEmpty) {
                                List<String> valueready = new ArrayList<>();
                                valueready.add("");
                                valueready.add("");
                                newContents.add(MultiValueAttribute.Factory.create(_fileSet, tag, false, valueready,
                                        false, false));
                            } else {
                                newContents.add(MultiValueAttribute.Factory.create(_fileSet, tag, false,
                                        new ArrayList<String>(vals.get(tag)), false, false));
                            }

                            row++;
                        }
                    }
                
            }
        }
        _contents.clear();
        _contents.addAll(newContents);
        fireTableDataChanged();
}
    
    private ArrayList<MultiValueAttribute> getDicomItemValues(DicomElement de, DicomObject ob, String ItemCount, int ItemTag, boolean DontItemcount, int counter) {
        ArrayList<MultiValueAttribute> valueready = new ArrayList<MultiValueAttribute>();
        ArrayList<String> valueofMv = new ArrayList<>();
        if(de!=null) {
        if(de.hasItems()) {
            for(int i =1;i<=de.countItems();i++) {
                counter++;
                if(!DontItemcount) {
                    String vorzeichen="";
                    for(int t = 0;t<counter;t++) {
                        vorzeichen+=">";
                    }
                    valueofMv.clear();
                    valueofMv.add(vorzeichen+"== Item "+ItemCount+i);
                    valueofMv.add("{Item}");
                    valueready.add(MultiValueAttribute.Factory.create(_fileSet, ItemTag,
                            false, valueofMv, true, false));
                }
                DicomObject ItemOb=de.getDicomObject(i-1);
                for(final Iterator<DicomElement> ei = ItemOb.datasetIterator(); ei.hasNext(); ) {
                    Integer tag = ei.next().tag();
                    DicomElement ItemDe = ItemOb.get(tag);
                    if(ItemOb.get(tag).toString().contains("SQ")) {
                        valueready.addAll(this.getDicomTagValues(ItemDe, ItemOb, tag, true, counter, ItemCount+i+"."));
                    }
                    else {
                    valueready.addAll(this.getDicomItemValues(ItemDe, ItemOb,ItemCount+i+".", tag, false, counter));
                    }
                }
            }
        }
        else {
            String vorzeichen="";
            for(int i = 0;i<counter;i++) {
                vorzeichen+=">";
            }
            valueofMv.clear();
            valueofMv.add(vorzeichen+org.dcm4che2.data.ElementDictionary.getDictionary().nameOf(de.tag()));
            valueofMv.add(de.getValueAsString(ob.getSpecificCharacterSet(), 1000));
            valueready.add(MultiValueAttribute.Factory.create(_fileSet, ItemTag,
                    false, valueofMv, true, false));
            }
        }
        return valueready;
    }
    private ArrayList<MultiValueAttribute> getDicomTagValues(DicomElement de, DicomObject ob, int tagDicomOb, boolean isSubSequence, int counter,String ItemCount) {
        ArrayList<MultiValueAttribute> valueready = new ArrayList<MultiValueAttribute>();
        ArrayList<String> valueofMv = new ArrayList<>();
        if(de!=null) {
        boolean beforeFirstItem = true;
        if(de.hasItems()) {
            if(beforeFirstItem) {
                counter++;
                String vorzeichenSequence="";
                for(int t = 0;t<counter;t++) {
                    vorzeichenSequence+=">";
                }
                valueofMv.clear();
                if(!isSubSequence) {
                    valueofMv.add(vorzeichenSequence+org.dcm4che2.data.ElementDictionary.getDictionary().nameOf(de.tag()));
                    valueofMv.add("{Sequence}");
                }
                else {
                    valueofMv.add(vorzeichenSequence+org.dcm4che2.data.ElementDictionary.getDictionary().nameOf(de.tag()));
                    valueofMv.add("{SubSequence}");
                }
                valueready.add(MultiValueAttribute.Factory.create(_fileSet, tagDicomOb,
                        false, valueofMv, true, false));
                beforeFirstItem=false;
            }
            for(int i =1;i<=de.countItems();i++) {
                String vorzeichen="";
                for(int t = 0;t<counter;t++) {
                    vorzeichen+=">";
                }
                valueofMv.clear();
                valueofMv.add(vorzeichen+ItemCount+i);
                valueofMv.add("{Item}");
                valueready.add(MultiValueAttribute.Factory.create(_fileSet, tagDicomOb,
                        false, valueofMv, true, false));
                DicomObject ItemOb=de.getDicomObject(i-1);
                for(final Iterator<DicomElement> ei = ItemOb.datasetIterator(); ei.hasNext();) {
                    Integer tag = ei.next().tag();
                    DicomElement ItemDe = ItemOb.get(tag);
                    if(ItemOb.get(tag).toString().contains("SQ")) {
                        valueready.addAll(this.getDicomTagValues(ItemDe, ItemOb, tag, true, counter, ItemCount+i+"."));
                    }
                    else {
                    valueready.addAll(this.getDicomItemValues(ItemDe, ItemOb, ItemCount+i+".", tag, false, counter));
                    }
                }
            }
        }
        else {
            valueofMv.clear();
            valueofMv.add(org.dcm4che2.data.ElementDictionary.getDictionary().nameOf(de.tag()));
            if(de.getValueAsString(ob.getSpecificCharacterSet(), 10000)==null) {
                valueofMv.add(""); 
            }
            else {
                valueofMv.add(de.getValueAsString(ob.getSpecificCharacterSet(), 10000));
            }
            valueready.add(MultiValueAttribute.Factory.create(_fileSet, tagDicomOb,
                    false, valueofMv, true, false));}
        }
        return valueready;
    }

    /**
     * Based on the current file selection, updates the list of selected files,
     * the attribute values for display, and the selected actions
     *
     * @param wipeTableFirst true if the existing table contents should be wiped
     *                       immediately rather than waiting for the first
     *                       update.  This isn't always obvious in theory,
     *                       so it mostly reflects playing around and seeing
     *                       where an immediate wipe seemed appropriate.
     */
    private void cacheValues(boolean wipeTableFirst) {
        if (_cachingProgress != null) {
            _cachingProgress.cancel();
        }

        final List<File> localSelectedFiles;
        synchronized (selectedFiles) {
            selectedFiles.clear();
            for (final TreePath tp : _fileSelections) {
                final DirectoryRecord dr = (DirectoryRecord) tp.getLastPathComponent();
                collectReferencedFiles(dr, selectedFiles);
            }
            localSelectedFiles = new ArrayList<>(selectedFiles);
        }

        if (wipeTableFirst) {
            _contents.clear();
        }

        long lastUpdate = new Date().getTime() - CONTENTS_UPDATE_INTERVAL + CONTENTS_SHOW_INTERVAL;

        _cachingProgress = _dicomBrowser.getStatusBar().getTaskMonitor(0, localSelectedFiles.size(),
                                                                       RESOURCE_BUNDLE.getString("reading-values"), RESOURCE_BUNDLE.getString("done"));
        assert _cachingProgress != null;

        final ProgressMonitorI             localTaskMonitor = _cachingProgress;
        final LinkedListMultimap<Integer, String> values           = LinkedListMultimap.create();

        int progress = 0;
        for (final File file : localSelectedFiles) {
            if (_cachingProgress == null) {
                break;
            }
            _cachingProgress.setNote(String.format(RESOURCE_BUNDLE.getString(READING_VALS_FORMAT), file.getName()));

            final Map<Integer, String> fv;
            try {
                final Map<Integer, ConversionFailureException> failures = new LinkedHashMap<>();
                fv = _fileSet.getValuesFromFile(file, 0, MAX_TAG, failures);
                if (failures.isEmpty()) {
                    // Build the values for the selected files with operations applied.
                    // Some operations may add new attributes, so the complete set of tags
                    // comes from both files and operations.
                    int row=0;
                    final SortedSet<Integer> tags = new TreeSet<>(fv.keySet());
                    for(int rowofOps:_operationsTags.keySet()) {
                        if(_operations.containsKey(rowofOps)&&_operations.get(rowofOps).containsKey(file)) {
                            if(_operations.get(rowofOps).get(file).toString().contains("Addition")) {
                                tags.add(_operationsTags.get(rowofOps));
                            }
                        } 
                    }
                    for (final int tag : tags) {
                        
                        final Map<File, Operation> tagOperations = _operations.get(row);
                        if (null != tagOperations && tagOperations.containsKey(file)) {
                            try {
                                final String v = tagOperations.get(file).apply(fv);
                                if(v==null&&!fv.containsKey(tag)) {
                                    values.put(tag, null == v ? RESOURCE_BUNDLE.getString("deleted") : v);
                                }
                                else {
                                    values.put(tag, fv.get(tag));
                                }
                            } catch (ScriptEvaluationException e) {
                                log.error("error applying script", e);
                            }
                        } else {
                            values.put(tag, fv.get(tag));
                        }
                        row++;
                    }
                } else {
                    log.error("Conversion errors reading {} : {}", file, failures);
                    continue; // move on to next file
                }
            } catch (IOException | SQLException e) {
                e.printStackTrace();
                log.error("error caching values", e);
                continue;     // move on to next file
            } catch (OutOfMemoryError e) {
                JOptionPane.showMessageDialog(_dicomBrowser, RESOURCE_BUNDLE.getString(OUT_OF_MEMORY_MESSAGE), RESOURCE_BUNDLE.getString(OUT_OF_MEMORY_TITLE), JOptionPane.ERROR_MESSAGE);
                break;
            } finally {
                if (localTaskMonitor.isCanceled()) {
                    fireTableDataChanged();
                    localTaskMonitor.close();
                    // TODO: needs synchronization?
                    if (localTaskMonitor == _cachingProgress) {
                        _cachingProgress = null;
                    }
                } else {
                    _cachingProgress.setProgress(++progress);
                }
            }

            if (_cachingProgress == null) {
                return;
            }
            if (new Date().getTime() - lastUpdate > CONTENTS_UPDATE_INTERVAL) {
                updateValues(values);
                lastUpdate = new Date().getTime();
            }
        }
        vals=values;
        updateValues(values);

        _cachingProgress.close();
        _cachingProgress = null;
    }

    /* (non-Javadoc)
     * @see javax.swing.event.TreeSelectionListener#valueChanged()
     */
    public void valueChanged(TreeSelectionEvent e) {
        final TreePath[] tps       = e.getPaths();
        boolean          needsWipe = false;
        for (int i = 0; i < tps.length; i++) {
            if (e.isAddedPath(i)) {
                _fileSelections.add(tps[i]);
            } else {
                needsWipe = true;       // some of the attributes now displayed may be gone
                _fileSelections.remove(tps[i]);
            }
        }

        // Change the table contents to reflect the new file selections.
        // This is a little time-consuming, so we do it in a separate thread.
        _executorService.execute(new CacheBuilder(needsWipe));
    }


    ExecutorService getExecutorService() {
        return _executorService;
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    public int getColumnCount() {
        return nCols;
    }

    @Override
    public String getColumnName(final int col) {
        return RESOURCE_BUNDLE.getString(COLUMN_NAMES.get(col));
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getRowCount()
     */
    public int getRowCount() {
        return _contents.size();
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    public Object getValueAt(int rowIndex, int columnIndex) {
        assert 0 <= rowIndex;

        synchronized (_contents) {
            // These weird conditions can happen if contents update between
            // the getRowCount() and the getValueAt(); don't sweat it.
            if (rowIndex >= _contents.size()) {
                return null;
            }
            final MultiValueAttribute row = _contents.get(rowIndex);
            if (null == row) {
                return null;
            }

            switch (columnIndex) {
                case 0:
                    return row.getTagString();
                case 1:
                    return row.getNameString();

                case ACTION_COLUMN: {
                    final int            tag = row.getTag();
                    final Set<Operation> ops = new HashSet<>();
                    synchronized (selectedFiles) {
                        for (final File file : selectedFiles) {
                            if (_operationsTags.containsKey(rowIndex)) {
                                ops.add(_operations.get(rowIndex).get(file));  // null here = KEEP
                            } else {
                                ops.add(null);       // implicit KEEP
                            }
                            if (ops.size() > 1) {
                                return OperationFactory.getMultipleName();
                            }
                        }
                    }
                    assert ops.size() == 1;
                    final Operation op = ops.iterator().next();
                    return null == op ? OperationFactory.getDefaultName() : op.getName();
                }

                case VALUE_COLUMN:
                    return row;

                default:
                    throw new IndexOutOfBoundsException("bad column index");
            }
        }
    }

    boolean allowClear(final ListSelectionModel lsm) {
        for (int i = lsm.getMinSelectionIndex(); i <= lsm.getMaxSelectionIndex(); i++) {
            if (lsm.isSelectedIndex(i)) {
                return _contents.get(i).isModifiable();
            }
        }
        return false;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return col == FileSetTableModel.VALUE_COLUMN && _contents.get(row).isModifiable();
    }

    @Override
    public void setValueAt(final Object object, final int row, final int col) {
        assert VALUE_COLUMN == col;
        if (object == null) {
            return;      // this can happen when an editing operation is canceled
        } else if (object instanceof Operation) {
            doOperation((Operation) object, row);
        } else if (object instanceof String) {
            final MultiValueAttribute attr = _contents.get(row);
            // If this attribute has a single value over the file selections,
            // and it hasn't changed, return without doing anything.
            if (attr.size() == 1 && StringUtils.equals(attr.toString(), (String) object)) {
                return;
            }
            
            Operation op = OperationFactory.getInstance(OperationFactory.ASSIGN,
                    attr.getTag(), (String)object);
            if(op.getName()=="Assign") {
                ((Assignment) op).setRow(row);
            }
            doOperation(op, row);
            
            /*
            final String value = (String) object;
            final int    tag   = attr.getTag();
            if (log.isDebugEnabled()) {
                log.debug("Creating new ASSIGN operation for tag ({}) with value {}", Integer.toHexString(tag), value);
            }
            doOperation(OperationFactory.getInstance(OperationFactory.ASSIGN, tag, value), row);
            */
            // if we're in the middle of building the values table,
            // we'll need to start over.
            if (_cachingProgress != null) {
                _executorService.execute(new CacheBuilder(false));
            }
        } else if (object instanceof MultiValueAttribute) { // only on canceled edit
            if (!_contents.get(row).equals(object)) {
                throw new IllegalArgumentException("setValueAt() given unknown attribute: " + object);
            }
        } else {
            throw new IllegalArgumentException("setValueAt() given unknown object type: " + object.getClass().getName());
        }
        fireTableRowsUpdated(row, row);     // needed to update Action
    }


    /**
     * Perform the indicated operations, update the table entries,
     * and add a single corresponding Command to the browser's Undo stack.
     *
     * @param operation The operation to do.
     */
    synchronized void doOperation(final Operation operation, int rowIndex) {
        // The following loop has the effect of clearing any previous
        // operations on the selected files, so any replaced operations
        // should be saved to put into the undo stack
        
        final Set<Integer> affectedTags = new TreeSet<>();
        if(operation.getName()=="Assign"||operation.getName()=="Clear") {
            ((Assignment) operation).setRow(rowIndex);
        }
        else if(operation.getName()=="Delete") {
            ((Deletion) operation).setRow(rowIndex);
        }
        else if(operation.getName()=="Keep") {
            ((Keep) operation).setRow(rowIndex);
        }
        

        // Assign the modified row contents
        // final int tag = op.getTagSubject();

        // Adding can only be done from AttributeAddDialog where -1 as a row is given
        if (rowIndex >= 0) {
            final Map<File, Operation> replacedOps = addOperation(operation, selectedFiles, affectedTags);
            _dicomBrowser.add(new Command(operation, selectedFiles, replacedOps, affectedTags));
            MultiValueAttribute mv = _contents.get(rowIndex);
            boolean applied = false;

            final int tag = _contents.get(rowIndex).getTag();

            if (operation instanceof Assignment) {
                // Our assignments are simple string assignments that don't depend
                // on other tags, so we can get the new value directly.
                String[] vals = mv.getValues();
                if (vals[0]!=null&&vals[0].startsWith(">")) {
                    try {
                        List<String> values = new ArrayList<String>();
                        values.add(vals[0]);
                        values.add(operation.apply(null));
                        _contents.set(rowIndex,
                                MultiValueAttribute.Factory.create(_fileSet, tag, true, values, true, false));
                    } catch (ScriptEvaluationException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        _contents.set(rowIndex,
                                MultiValueAttribute.Factory.create(_fileSet, tag, true, operation.apply(null), false));
                    } catch (ScriptEvaluationException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else if (operation instanceof Deletion) {
                // Attribute deletion is even simpler.
                _contents.set(rowIndex, MultiValueAttribute.Factory.create(_fileSet, tag, true, "{Deleted}", false));
            } else if (operation instanceof Keep) {
                // Keep is sort of complicated and require a values rebuild.
                // This is time-consuming, so we do it in a separate thread.
                _contents.set(rowIndex, null);
                _executorService.execute(new CacheBuilder(false));
            } else {
                throw new UnsupportedOperationException("unknown operation " + operation);
            }
            applied = true;
            fireTableRowsUpdated(rowIndex, rowIndex);
        } else {
            // The only way an operation can be applied to a nonexistent row
            // is if we're creating a new row. Insert the new one in its correct
            // place (in increasing tag order).
            assert operation instanceof Addition;
            String appliedValue;
            try {
                appliedValue = operation.apply(null);
            } catch (ScriptEvaluationException e) {
                appliedValue = null;
            }

            final int insertTag = operation.getTopTag();
            final int lastTag = _contents.get(_contents.size() - 1).getTag();
            final MultiValueAttribute value = MultiValueAttribute.Factory.create(_fileSet, insertTag, true,
                    appliedValue, false);

            int newRow = -1;
            if (insertTag > lastTag) {
                newRow = _contents.size();
                _contents.add(value);
            } else {
                for (int row = 0; row < _contents.size(); row++) {
                    final int tag = _contents.get(row).getTag();
                    if (tag > insertTag) {
                        _contents.add(row, value);
                        newRow = row;
                        break;
                    }
                }
            }
            Map<Integer, Map<File, Operation>> _operationsnew     = new LinkedHashMap<>();
            Map<Integer, Integer> _operationsTagsnew = new LinkedHashMap<>();
            for(int keyrow:_operations.keySet()) {
                if(keyrow>=newRow) {
                    Map<File, Operation> fileops=_operations.get(keyrow);
                    Map<File, Operation> newfileops=new LinkedHashMap<>();
                    for(Entry<File, Operation> op:fileops.entrySet()) {
                        Operation ops=op.getValue();
                        if(op.getValue().getName()=="Assign"||op.getValue().getName()=="Clear") {
                            ((Assignment) ops).setRow(keyrow+1);
                        } else if(op.getValue().getName()=="Delete") {
                             ops=op.getValue();
                            ((Deletion) ops).setRow(keyrow+1);
                        }else if(op.getValue().getName()=="Keep") {
                             ops=op.getValue();
                            ((Keep) ops).setRow(keyrow+1);
                        }else if(op.getValue().getName()=="Addition") {
                             ops=op.getValue();
                            ((Addition) ops).setRow(keyrow+1);
                        }
                        newfileops.put(op.getKey(),ops);
                    }
                    _operationsnew.put(keyrow+1, newfileops);
                    _operationsTagsnew.put(keyrow+1, _operationsTags.get(keyrow));
                }
                else {
                    Map<File, Operation> fileops=_operations.get(keyrow);
                    _operationsnew.put(keyrow, fileops);
                    _operationsTagsnew.put(keyrow, _operationsTags.get(keyrow));

                }
            }
            _operations.clear();
            _operations.putAll(_operationsnew);
            _operationsTags.clear();
            _operationsTags.putAll(_operationsTagsnew);
            
            ((Addition) operation).setRow(newRow);
            Map<File, Operation> _replacedOps = addOperation(operation, selectedFiles, affectedTags);
            _dicomBrowser.add(new Command(operation, selectedFiles, _replacedOps, affectedTags));
            fireTableRowsInserted(newRow, newRow);
        }
    }

    /**
     * Builds a record of replaced operations. The caller is responsible for updating the table.
     *
     * @param operation    Operation to be applied
     * @param files        The files to be operated on
     * @param affectedTags The DICOM tags to be operated on
     *
     * @return Map of replaced Operations and the File to which they applied.
     */
    private Map<File, Operation> addOperation(final Operation operation, final Iterable<File> files, final Set<Integer> affectedTags) {
        final Map<File, Operation> replaced = new HashMap<>();
        boolean                    applied  = false;
        int rowAffected=0;
        if(operation.getName()=="Assign"||operation.getName()=="Clear") {
            rowAffected=((Assignment) operation).getRow();
        }
        else if(operation.getName()=="Delete") {
            rowAffected=((Deletion) operation).getRow();
        }
        else if(operation.getName()=="Keep") {
            rowAffected=((Keep) operation).getRow();
        }
        else if(operation.getName()=="Addition") {
            rowAffected=((Addition) operation).getRow();
        }
        int row=0;
        for (final MultiValueAttribute content : _contents) {
            final int tag = content.getTag();
            if (operation.affects(tag)&&row == rowAffected) {
                replaced.putAll(addOperationSequence(operation, files, replaced, tag, rowAffected));
                if (null != affectedTags && _operations.containsKey(rowAffected)) {
                    affectedTags.add(tag);
                }
                applied = true;
                break;
            }
            row++;
        }
        if (!applied) {
            addOperationSequence(operation, files, rowAffected);
        }
        return replaced;
    }

    private void addOperationSequence(final Operation operation, final Iterable<File> files, int row) {
        addOperationSequence(operation, files, null, operation.getTopTag(), row);
    }

    private Map<File, Operation> addOperationSequence(final Operation operation, final Iterable<File> files, final Map<File, Operation> replaced, final int tag, int row) {
        Map<File, Operation> fileOperations=new HashMap<>();
        boolean isAddition=false;
        if (!_operations.containsKey(row)) {
            _operations.put(row, fileOperations = new HashMap<>());
            _operationsTags.put(row, tag);
        } else {
            for(File file:files) {
                if(_operations.get(row).get(file).toString().contains("Addition")){
                    JOptionPane.showMessageDialog(_dicomBrowser.getFrame(),
                            "Can not add an Operation to and unsaved added Addition", "Error", JOptionPane.ERROR_MESSAGE);
                    isAddition=true;
                    break;
                }
            }
            if(!isAddition) {
                fileOperations = _operations.get(row);
            }
        }
        if (!isAddition) {
            for (final File file : files) {
                if (replaced != null && fileOperations.containsKey(file)) {
                    replaced.put(file, fileOperations.get(file));
                }
                fileOperations.put(file, operation);
            }
        }
        return replaced;
    }

    @SuppressWarnings("SameParameterValue")
    private Map<File, Operation> addOperation(final Operation op, final File file, final Set<Integer> affectedTags) {
        return addOperation(op, Collections.singleton(file), affectedTags);
    }

    Command addOperations(final String name, final int... rows) {
        if (rows.length == 0) {
            throw new IllegalArgumentException("operation requires at least one selected attribute");
        }

        final Operation[]                        ops      = new Operation[rows.length];
        final Map<Integer, Map<File, Operation>> replaced = new HashMap<>();

        for (int i = 0; i < rows.length; i++) {
            final int tag = _contents.get(rows[i]).getTag();       // TODO: may require translation if sorter is applied
            ops[i] = OperationFactory.getInstance(name, tag);
            if(name=="Delete") {
                ((Deletion) ops[i]).setRow(rows[i]);
            }
            else if(name=="Assign"||name=="Clear"){
                ((Assignment) ops[i]).setRow(rows[i]);
            }
            else if(name=="Keep"){
                ((Keep) ops[i]).setRow(rows[i]);
            }
            if (replaced.containsKey(i)) {
                throw new IllegalArgumentException("multiple operations specified for tag " + TagUtils.toString(tag));
            }
            replaced.put(i, addOperation(ops[i], selectedFiles, null));
        }
        
        final Command command = new Command(ops, selectedFiles, replaced);
        refreshAfterCommand(command);

        return command;
    }


    /**
     * Applies the given parsed script.
     *
     * @param statements   Statements parsed from anonymization script
     * @param onlySelected if true, script is applied only to selected files; otherwise, to the whole file set
     *
     * @return Command representing this script action
     * @throws CloneNotSupportedException 
     */
    Command doScript(final Iterable<Statement> statements, final boolean onlySelected, ProgressMonitor pm) throws CloneNotSupportedException {
        if (null == statements) {
            return null;
        }

        final Collection<File> files;
        try {
            files = onlySelected ? selectedFiles : _fileSet.getDataFiles();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(_dicomBrowser.getFrame(),
                                          "Error getting file list: " + e.getMessage(),
                                          e.getMessage(), JOptionPane.ERROR_MESSAGE);
            return null;    // TODO: is this okay?
        }
        final Set<Integer> rowsList = new LinkedHashSet<>();
        final Map<Integer, Map<File, Operation>> replaced = new HashMap<>();
        final Map<Integer, Map<File, Operation>> ops = new HashMap<>();
        //Map<File, Operation> entrie = new LinkedHashMap<>();


        int progress = 0;
        pm.setMinimum(0);
        pm.setMaximum(files.size());
        pm.setProgress(progress);

        for (final File file : files) {
            try {
                for (final Object opo : Statement.getOperations(statements, file)) {
                    final Operation op = (Operation) opo;
                    int row=0;
                    for (MultiValueAttribute content :_contents) {
                        final int tag = content.getTag();
                        if (op.affects(tag)) {
                            if(op.getName()=="Assign"||op.getName()=="Clear") {
                               ((Assignment) op).setRow(row);
                            }
                            else if(op.getName()=="Delete") {
                                ((Deletion) op).setRow(row);                      
                                }
                            else if(op.getName()=="Keep") {
                                ((Keep) op).setRow(row);                      
                                }
                            else if(op.getName()=="Addition"){
                                ((Addition) op).setRow(row);                           
                                }
                            rowsList.add(row);
                            replaced.put(row, addOperation(op.clone(), file, null));
                        }
                        row++;
                    }
                }
            } catch (IOException e) { // TODO: localize
                JOptionPane.showMessageDialog(_dicomBrowser.getFrame(),
                                              "Error reading file " + file.getName() + ": " + e.getMessage(),
                                              e.getMessage(), JOptionPane.ERROR_MESSAGE);
            } catch (ScriptEvaluationException e) {
                JOptionPane.showMessageDialog(_dicomBrowser.getFrame(),
                                              "Error applying script: " + e.getMessage(),
                                              e.getMessage(), JOptionPane.ERROR_MESSAGE);
            }
            pm.setProgress(++progress);
        }
        for(Integer row:rowsList) {
            if(_operations.containsKey(row)) {
                ops.put(row, _operations.get(row));
            }
        }

        final Command command = new Command(ops, replaced);
        refreshAfterCommand(command);

        return command;
    }

    /**
     * If the given Command affects any currently selected files,
     * rebuild the table.
     *
     * @param command The command to test.
     */
    private void refreshAfterCommand(final Command command) {
        final Set<File> files = new HashSet<>(command.getAllFiles());
        for (final Map<File, Operation> map : command.getReplaced().values()) {
            files.addAll(map.keySet());
        }

        files.retainAll(selectedFiles);

        if (!files.isEmpty()) {
            _executorService.execute(new CacheBuilder(false));
        }
    }


    /**
     * Does a Command.  (This is probably always a Redo.)
     *
     * @param command The command to execute.
     */
    void redo(final Command command) {
        final Operation[] ops = command.getOperations();
        for (int i = 0; i < ops.length; i++) {
            if(ops[i].toString().contains("Addition")) {
                //redo the rowedditing-todo: maybe put this edditing and the one in doOperation to addOperations to simplify as is basically the same code or create a method where the amount of rows and +- is the parameter
                Map<Integer, Map<File, Operation>> _operationsnew     = new LinkedHashMap<>();
                Map<Integer, Integer> _operationsTagsnew = new LinkedHashMap<>();
                for(int keyrow:_operations.keySet()) {
                    if(keyrow>=((Addition) ops[i]).getRow()) {
                        Map<File, Operation> fileops=_operations.get(keyrow);
                        Map<File, Operation> newfileops=new LinkedHashMap<>();
                        for(Entry<File, Operation> op:fileops.entrySet()) {
                            Operation operation=op.getValue();
                            if(op.getValue().getName()=="Assign"||op.getValue().getName()=="Clear") {
                                ((Assignment) operation).setRow(keyrow+1);
                            } else if(op.getValue().getName()=="Delete") {
                                operation=op.getValue();
                                ((Deletion) operation).setRow(keyrow+1);
                            }else if(op.getValue().getName()=="Keep") {
                                operation=op.getValue();
                                ((Keep) operation).setRow(keyrow+1);
                            }else if(op.getValue().getName()=="Addition") {
                                operation=op.getValue();
                                ((Addition) operation).setRow(keyrow+1);
                            }
                            newfileops.put(op.getKey(),operation);
                        }
                        _operationsnew.put(keyrow+1, newfileops);
                        _operationsTagsnew.put(keyrow+1, _operationsTags.get(keyrow));
                    }
                    else {
                        Map<File, Operation> fileops=_operations.get(keyrow);
                        _operationsnew.put(keyrow, fileops);
                        _operationsTagsnew.put(keyrow, _operationsTags.get(keyrow));

                    }
                }
                _operations.clear();
                _operations.putAll(_operationsnew);
                _operationsTags.clear();
                _operationsTags.putAll(_operationsTagsnew);
            }
            addOperation(ops[i], Arrays.asList(command.getFiles(i)), null);
        }
        refreshAfterCommand(command);
    }


    /**
     * Undoes a Command.
     *
     * @param command The command to undo.
     */
    void undo(final Command command) {
        // Clear operation
        final Set<Map.Entry<File, Operation>> restoredOps = new LinkedHashSet<>();

        final Operation[] ops = command.getOperations();
        boolean isaddition = false;
        int rowAffected=0;
        for (int i = 0; i < ops.length; i++) {
            if(ops[i].getName()=="Assign"||ops[i].getName()=="Clear") {
                rowAffected=((Assignment) ops[i]).getRow();
            }
            else if(ops[i].getName()=="Delete") {
                rowAffected=((Deletion) ops[i]).getRow();
            }
            else if(ops[i].getName()=="Addition") {
                rowAffected=((Addition) ops[i]).getRow();
                isaddition=true;
            }
            int row=0;
            
            for (final MultiValueAttribute attribute : _contents) {
                final int tag = attribute.getTag();
                if (ops[i].affects(tag) && row == rowAffected) {
                    for (final File file : command.getFiles(i)) {
                        _operations.get(row).remove(file);
                        if (_operations.get(row).isEmpty()) {
                            _operations.remove(row);
                            _operationsTags.remove(row);
                        }
                    }
                }
                row++;
            }
            //undo the change of the rows caused by the addition
            if (isaddition) {
                Map<Integer, Map<File, Operation>> _operationsnew = new LinkedHashMap<>();
                Map<Integer, Integer> _operationsTagsnew = new LinkedHashMap<>();
                for (int keyrow : _operations.keySet()) {
                    if (keyrow >= rowAffected) {
                        Map<File, Operation> fileops = _operations.get(keyrow);
                        Map<File, Operation> newfileops = new LinkedHashMap<>();
                        for (Entry<File, Operation> op : fileops.entrySet()) {
                            Operation oper = op.getValue();
                            if (op.getValue().getName() == "Assign" || op.getValue().getName() == "Clear") {
                                ((Assignment) oper).setRow(keyrow - 1);
                            } else if (op.getValue().getName() == "Delete") {
                                oper = op.getValue();
                                ((Deletion) oper).setRow(keyrow - 1);
                            } else if (op.getValue().getName() == "Keep") {
                                oper = op.getValue();
                                ((Keep) oper).setRow(keyrow - 1);
                            } else if (op.getValue().getName() == "Addition") {
                                oper = op.getValue();
                                ((Addition) oper).setRow(keyrow - 1);
                            }
                            newfileops.put(op.getKey(), oper);
                        }
                        _operationsnew.put(keyrow-1, newfileops);
                        _operationsTagsnew.put(keyrow-1, _operationsTags.get(keyrow));
                    } else {
                        Map<File, Operation> fileops = _operations.get(keyrow);
                        _operationsnew.put(keyrow, fileops);
                        _operationsTagsnew.put(keyrow, _operationsTags.get(keyrow));

                    }
                }
                _operations.clear();
                _operations.putAll(_operationsnew);
                _operationsTags.clear();
                _operationsTags.putAll(_operationsTagsnew);
            }
           
        
        
            
        }
        for(int row: _operations.keySet()) {
            restoredOps.addAll(_operations.get(row).entrySet());
        }
        refreshAfterCommand(command);
    }

    public int size() throws SQLException {
        return _fileSet.size();
    }


    /**
     * Build a list of Statements equivalent to our operation map
     *
     * @return head of Statement list
     */
    private List<Statement> buildStatements() {
        // Build a list of Statements equivalent to our operation map
        final SetMultimap<Operation,File> ops = LinkedHashMultimap.create();
        for (final Map<File,Operation> tagops : _operations.values()) {
            for (final Map.Entry<File,Operation> e : tagops.entrySet()) {
                ops.put(e.getValue(), e.getKey());
            }
        }

        final List<Statement> statements = Lists.newArrayList();
        for (final Operation op : ops.keySet()) {
            statements.add(new Statement(new Constraint(null, ops.get(op)), op));
        }
        return statements;
    }

    /**
     * Send the modified files (all or only the selected files) to a
     * DICOM receiver.
     *
     * @param host          DNS name of the remote host
     * @param port          TCP port of the remote host
     * @param remoteAeTitle DICOM application entity name of the remote host
     * @param localAeTitle  DICOM application entity name of the remote host
     * @param isTls         Indicates whether the C-STORE connection should use TLS encryption
     * @param allFiles      true if all files in the file set should be sent
     */
    void send(final String host, final String port, final String remoteAeTitle, final String localAeTitle, final boolean isTls, final boolean allFiles) {
        try {
            final Set<File>                files;
            final List<TransferCapability> transferCapabilities;
            synchronized (this) {
                if (allFiles) {
                    files = _fileSet.getDataFiles();
                    transferCapabilities = _fileSet.getTransferCapabilities(TransferCapability.SCU);
                } else {
                    files = selectedFiles;
                    transferCapabilities = _fileSet.getTransferCapabilities(TransferCapability.SCU, selectedFiles);
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("Requested transfer capabilities:\n{}",
                          transferCapabilities.stream().map(transferCapability ->
                                                                " * SOP Class " + transferCapability.getSopClass() + " " + transferCapability.getRole() +
                                                                "\n   TSUIDs " + StringUtils.join(transferCapability.getTransferSyntax())).collect(Collectors.joining("\n")));
            }

            final DicomObjectExporter exporter = new CStoreExporter(host, port, isTls, remoteAeTitle, localAeTitle, transferCapabilities);
            exportFileBatch(files, exporter, SENDING_FILES);
        } catch (SQLException e) {
            log.error("unable to get data files", e);
            JOptionPane.showMessageDialog(_dicomBrowser.getFrame(),      // TODO: localize
                                          "Unable to retrieve data files from database: " + e.getMessage(),
                                          "Send failed",
                                          JOptionPane.ERROR_MESSAGE);
        }
    }

    private void exportFileBatch(final Set<File> files, final DicomObjectExporter exporter, final String sendingFiles) {
        final EditProgressMonitor progressMonitor = SwingProgressMonitor.getMonitor(_dicomBrowser.getFrame(), RESOURCE_BUNDLE.getString(sendingFiles), "", 0, files.size());
        final BatchExporter       batch           = new BatchExporter(exporter, buildStatements(), files);
        batch.setProgressMonitor(progressMonitor, 0);
        _executorService.execute(new ExportFailureHandler(batch, _dicomBrowser.getFrame()));
    }

    private void export(final DicomObjectExporter exporter, final boolean allFiles) {
        final SortedSet<File> files;
        try {
            files = new TreeSet<>(allFiles ? _fileSet.getDataFiles() : selectedFiles);
        } catch (SQLException e) {
            log.error("unable to get data files", e);
            JOptionPane.showMessageDialog(_dicomBrowser.getFrame(),      // TODO: localize
                                          "Unable to retrieve data files from database: " + e.getMessage(),
                                          "Export failed",
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }

        exportFileBatch(files, exporter, WRITING_FILES);
    }

    public void saveInAdjacentDir(final String format, final boolean allFiles) {
        export(new AdjacentDirFileExporter(AE_TITLE, format), allFiles);
    }

    public void saveInAdjacentFile(final String suffix, final boolean allFiles) {
        export(new AdjacentFileExporter(AE_TITLE, suffix), allFiles);
    }

    public void saveInNewRoot(final String rootPath, final boolean allFiles) {
        final DicomObjectExporter exporter;
        try {
            exporter = new NewRootFileExporter(AE_TITLE, new File(rootPath), _fileSet.getRoots());
        } catch (Exception e) {
            log.error("unable to resolve data roots", e);
            JOptionPane.showMessageDialog(_dicomBrowser.getFrame(),      // TODO: localize
                                          "Error in organizing output files: " + e.getMessage(),
                                          "Export failed",
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }
        export(exporter, allFiles);
    }

    public void overwrite(final boolean allFiles) {
        export(new OverwriteFileExporter(AE_TITLE), allFiles);
    }
    
    public int getTagAtRow(int row) {
        return _contents.get(row).getTag();
    }
}