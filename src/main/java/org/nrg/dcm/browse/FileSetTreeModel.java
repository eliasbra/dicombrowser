/**
 * Copyright (c) 2006,2007,2010 Washington University
 */

package org.nrg.dcm.browse;

import org.nrg.dcm.db.DirectoryRecord;
import org.nrg.dcm.db.FileSet;
import org.nrg.dcm.db.ProgressMonitorI;

import javax.annotation.Nonnull;
import javax.swing.*;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.io.File;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public final class FileSetTreeModel implements TreeModel {
    private static final String READING_FILES       = "Reading files...";
    private static final String UNABLE_TO_LOAD_FILE = "Unable to load file: %1$s";
    private static final String IMPORT_FAILED       = "Import failed";

    private static final ResourceBundle rsrcb = new ListResourceBundle() {
        @Override
        public Object[][] getContents() {
            return contents;
        }

        private final Object[][] contents = {       // TODO: LOCALIZE THIS
                                                    {READING_FILES, READING_FILES},
                                                    {UNABLE_TO_LOAD_FILE, UNABLE_TO_LOAD_FILE},
                                                    {IMPORT_FAILED, IMPORT_FAILED},
                                                    };
    };

    private final Component                     _window;
    private final FileSet                       _fileSet;
    private final List<DirectoryRecord>         _patients  = new LinkedList<>();
    private final Collection<TreeModelListener> _listeners = new HashSet<TreeModelListener>();

    FileSetTreeModel(final Component window, final @Nonnull FileSet fileSet) {
        _window = window;
        _fileSet = fileSet;
        handleAdd();
    }


    private void handleAdd() {
        _patients.clear();
        _patients.addAll(_fileSet.getPatientDirRecords());
        final TreeModelEvent e = new TreeModelEvent(this, new Object[] {_fileSet});    // TODO: permit treeNodesInserted
        for (TreeModelListener l : _listeners) {
            l.treeStructureChanged(e);
        }
    }


    /**
     * Add the listed files to the FileSet.  Must be invoked from outside the Swing event handler.
     *
     * @param files
     */
    public void add(final Collection<File> files) {
        assert !SwingUtilities.isEventDispatchThread();

        final ProgressMonitorI pn = SwingProgressMonitor.getMonitor(_window, rsrcb.getString(READING_FILES),
                                                                    "", 0, files.size());

        try {
            _fileSet.add(files, pn);
            handleAdd();
        } catch (final Exception e) {
            SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(_window,
                                                                           String.format(rsrcb.getString(UNABLE_TO_LOAD_FILE), e.getMessage()),
                                                                           rsrcb.getString(IMPORT_FAILED),
                                                                           JOptionPane.ERROR_MESSAGE));
        } finally {
            pn.close();
        }
    }

    /**
     * Remove the selection from the FileSet.
     *
     * @param treePaths Paths to be removed
     */
    void remove(final Collection<TreePath> treePaths) {
        _fileSet.remove(treePaths.stream().map(TreePath::getLastPathComponent).filter(component -> component instanceof DirectoryRecord).map(record -> (DirectoryRecord) record).collect(Collectors.toList()));

        final Collection<TreeModelEvent> es = new LinkedList<>();
        for (final TreePath tp : treePaths) {
            es.add(new TreeModelEvent(this, tp.getParentPath()));    // TODO: permit treeNodesRemoved
        }

        // Separate extracting the TreePaths from notifying listeners; one of the
        // listeners may be the caller, and may modify treePaths as a result of the
        // notification.
        for (TreeModelEvent e : es) {
            for (TreeModelListener l : _listeners) {
                l.treeStructureChanged(e);
            }
        }
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#addTreeModelListener(javax.swing.event.TreeModelListener)
     */
    public void addTreeModelListener(final TreeModelListener l) {
        _listeners.add(l);
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#getChild(java.lang.Object, int)
     */
    public Object getChild(final Object parent, final int index) {
        if (parent == _fileSet) {
            return _patients.get(index);
        }
        assert parent instanceof DirectoryRecord;
        assert 0 <= index && index <= ((DirectoryRecord) parent).getLower().size();
        return ((DirectoryRecord) parent).getLower().get(index);
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
     */
    public int getChildCount(final Object parent) {
        return (parent == _fileSet) ? _patients.size() : ((DirectoryRecord) parent).getLower().size();
    }

    @Override
    public int getIndexOfChild(final Object parent, final Object child) {
        final DirectoryRecord record = (DirectoryRecord) child;
        if (parent == _fileSet) {
            return _patients.indexOf(record);
        } else {
            return ((DirectoryRecord) parent).getLower().indexOf(record);
        }
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#getRoot()
     */
    public Object getRoot() {
        return _fileSet;
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
     */
    public boolean isLeaf(final Object node) {
        if (!(node instanceof DirectoryRecord)) {
            return false;
        }
        return ((DirectoryRecord) node).getType() == DirectoryRecord.Type.INSTANCE;
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#removeTreeModelListener(javax.swing.event.TreeModelListener)
     */
    public void removeTreeModelListener(final TreeModelListener l) {
        _listeners.remove(l);
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
     */
    public void valueForPathChanged(final TreePath arg0, final Object arg1) {
        assert false : "don't change contents of this tree!";
    }

    public File[] getSelectedFiles() {
        return null;
    }
}
