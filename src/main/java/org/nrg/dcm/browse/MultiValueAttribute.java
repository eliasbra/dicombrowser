/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.browse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.nrg.dcm.db.FileSet;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface MultiValueAttribute extends Iterable<String> {
    int getTag();

    int size();

    String[] getValues();

    String getTagString();

    String getNameString();

    boolean isModified();

    boolean isModifiable();
    
    boolean isDeleted();

    public class Factory {
        public final static MultiValueAttribute
        create(final FileSet fs, final int tag, final boolean isModified, final List<String> values, boolean isSequence, boolean deleted) {
            if (fs.isSequenceTag(tag)||isSequence) {
                return new SQValueAttribute(tag, isModified, values, deleted);
            } else {
                return new SimpleMultiValueAttribute(tag, isModified, values, deleted);
            }
        }

        public final static MultiValueAttribute
        create(final FileSet fs, final int tag, final boolean isModified, final String value, boolean deleted) {
            List<String> values = new ArrayList<String>();
            values.add(value);
            return create(fs, tag, isModified, values, false, deleted);
        }

        public final static MultiValueAttribute
        create(final FileSet fs, final int tag, final boolean isModified, boolean deleted) {
            return create(fs, tag, isModified, new ArrayList<String>(), false, deleted);
        }
    }
}
