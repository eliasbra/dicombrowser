/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.browse;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.util.TagUtils;
import org.nrg.dcm.edit.TagPattern;
import org.nrg.dcm.tags.PrivateTagChecker;

import com.google.common.collect.Lists;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class SQValueAttribute implements MultiValueAttribute {
    private static final DicomObject dcmo = new BasicDicomObject();
    private static final String DISPLAY = "{sequence}";
    private static final String DELETED_DISPLAY = "{deleted sequence}";
    private final int tag;
    private boolean deleted = false;
    private boolean isModified;
    private final List<String> values;
    private TagPattern tp = null;

    public SQValueAttribute(final int tag, final boolean isModified,final Iterable<String> values, boolean deleted) {
        this.tag = tag;
        this.isModified = isModified;
        this.values=Lists.newArrayList(values);
        this.deleted = deleted;
    }

    /* (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    public Iterator<String> iterator() { return values.iterator(); }

    /* (non-Javadoc)
     * @see org.nrg.dcm.browse.MultiValueAttribute#getTag()
     */
    public int getTag() { return tag; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.browse.MultiValueAttribute#size()
     */
    public int size() { return 1; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.browse.MultiValueAttribute#getValues()
     */
    public String[] getValues() { return values.toArray(new String[0]); }

    /* (non-Javadoc)
     * @see org.nrg.dcm.browse.MultiValueAttribute#getTagString()
     */
    public String getTagString() { return TagUtils.toString(tag); }

    /* (non-Javadoc)
     * @see org.nrg.dcm.browse.MultiValueAttribute#getNameString()
     */
    public String getNameString() {
        if(values.get(0).contains("?")) {
            tp = new TagPattern(tag);
            String tagname = PrivateTagChecker.getNameTag(tp.getPattern());
            String vorzeichen = values.get(0).substring(0, values.get(0).length()-1);
            return vorzeichen+tagname;
        }
        else if(!values.get(0).isEmpty()) {
            return values.get(0);
        }
        else {
            return dcmo.nameOf(tag);
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.browse.MultiValueAttribute#isModified()
     */
    public boolean isModified() { return isModified; }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.browse.MultiValueAttribute#isModifiable()
     */
    public boolean isModifiable() { if(values.get(1)!="{Sequence}"&&values.get(1)!="{Item}"&&values.get(1)!="{SubSequence}"&&!deleted) {return true; }else{return false;}}

    public boolean isDeleted() {return deleted;}
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.browse.MultiValueAttribute#set(java.lang.String)
     */
    public void modify(String v) {
        isModified=true;
        values.add(0, v);
        values.remove(1);
    }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.browse.MultiValueAttribute#delete()
     */
    public void delete() {
        deleted = true;
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if(deleted) {return "{deleted}";};
        if(values.size()<2) return "Empty";
            return values.get(1);
    }
}
