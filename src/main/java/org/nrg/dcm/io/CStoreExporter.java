/*
 * Copyright (c) 2009 Washington University
 */

package org.nrg.dcm.io;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.net.*;
import org.dcm4che2.net.NetworkConnectionBuilder.TlsType;
import org.nrg.dcm.DicomSender;
import org.nrg.dcm.edit.DicomUtils;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.File;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;


/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 */
public class CStoreExporter implements DicomObjectExporter {
    private static final String DEFAULT_AE_TITLE = "NRG C-STORE SCU";
    private static final String PROTOCOL_TLS     = "TLS";

    private final static TrustManager[] yesManagers = new TrustManager[] {
        new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] chain, String authType) { /* always accept */ }

            public void checkServerTrusted(X509Certificate[] chain, String authType) { /* always accept */ }

            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};

    private CStoreExporter(final DicomSender dicomSender) {
        _dicomSender = dicomSender;
    }

    public CStoreExporter(final String host, final String port,
                          final boolean isTLS, final String remoteAeTitle, final String localAeTitle,
                          final TransferCapability transferCapability) {
        this(host, port, isTLS, remoteAeTitle, localAeTitle, Collections.singletonList(transferCapability));
    }

    public CStoreExporter(final String host, final String port,
                          final boolean isTLS, final String remAETitle, final String locAETitle,
                          final List<TransferCapability> transferCapabilities) {
        this(buildSender(host, port, remAETitle, locAETitle, transferCapabilities, isTLS ? TlsType.AES : null, false, yesManagers));
    }

    private static DicomSender buildSender(final String remHost, final String remPort,
                                           final String remAETitle, final String locAETitle,
                                           final List<TransferCapability> transferCapabilities, final TlsType tlsType,
                                           final boolean needsClientAuth, final TrustManager[] trustManagers) {
        final NetworkConnection networkConnection;
        if (null != tlsType) {
            networkConnection = new NetworkConnectionBuilder()
                .setTls(tlsType)
                .setTlsNeedClientAuth(needsClientAuth)
                .build();
        } else {
            networkConnection = new NetworkConnection();
        }

        final NetworkApplicationEntity localAE = new NetworkApplicationEntityBuilder()
            .setAETitle(null == locAETitle ? DEFAULT_AE_TITLE : locAETitle)
            .setTransferCapability(transferCapabilities.toArray(new TransferCapability[0]))
            .setNetworkConnection(networkConnection)
            .build();

        final NetworkConnectionBuilder rncb = new NetworkConnectionBuilder();
        rncb.setHostname(remHost).setPort(Integer.parseInt(remPort));
        if (null != tlsType) {
            rncb.setTls(tlsType).setTlsNeedClientAuth(needsClientAuth);
        }

        final NetworkApplicationEntity remoteAE = new NetworkApplicationEntityBuilder()
            .setNetworkConnection(rncb.build())
            .setAETitle(remAETitle)
            .build();

        final DicomSender sender = new DicomSender(localAE, remoteAE);
        if (null != tlsType) {
            try {
                final SSLContext context = SSLContext.getInstance(PROTOCOL_TLS);
                context.init(null, trustManagers, null);
                sender.setSSLContext(context);
            } catch (NoSuchAlgorithmException | KeyManagementException e) {
                throw new RuntimeException(e);    // programming error
            }
        }
        return sender;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.io.DicomObjectExporter#close()
     */
    public void close() {
        _dicomSender.close();
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.io.DicomObjectExporter#export(org.dcm4che2.data.DicomObject, java.io.File)
     */
    public void export(final DicomObject o, final File source) throws Exception {
        _dicomSender.send(o, DicomUtils.getTransferSyntaxUID(o));
    }

    private final DicomSender _dicomSender;
}
