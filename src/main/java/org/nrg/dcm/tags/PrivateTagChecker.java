package org.nrg.dcm.tags;

public class PrivateTagChecker {
    static notModifiableTags notmodifiabletag = new notModifiableTags();
    
    public static boolean isNotDeletable(int tag) {
        return notmodifiabletag.notDeletable(tag);
    }
    
    public static boolean notAssignable(int tag) {
        return notmodifiabletag.notAssignable(tag);
    }
    
    //with this method you can easily add private tags and show their name
    //create a class for a Group of Tags and create for each tag a variable like following: private static final int TEST_TAG_NAME = 0x00020012; for the tag (0002,0012)
    //in that class create a method and an instance of the class where for the method the tag is a parameter an make a switch(tag & 0xFFFF00FF) and the cases as the declared tags; 
    //now create an instance of this class here and call the method
    //this is also possible with multiple classes to group private tags
    public static String getNameTag(int tag) {
        return "?";
    }
}
