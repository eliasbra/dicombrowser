package org.nrg.dcm.tags;

public class notModifiableTags {
    //(0002,0000)
    public static final int FILE_META_INFORMATION_LENGTH = 0x00020000;

    //(0002,0001)
    public static final int FILE_META_INFORMATION_VERSION = 0x00020001;
    
    //(0002,0002)
    public static final int MEDIA_STORAGE_SOP_CLASS_UID = 0x00020002;

    //(0002,0003)
    public static final int MEDIA_STORAGE_SOP_INSTANCE_UID = 0x00020003;

    //(0002,0010)
    public static final int TRANSFER_SYNTAX_UID = 0x00020010;
    
    //(0002,0012)
    public static final int IMPLEMENTATION_CLASS_UID = 0x00020012;

    //(0002,0013)
    public static final int IMPLEMENTATION_VERSION_NAME = 0x00020013;

    //(0002,0016)
    public static final int SOURCE_APPLICATION_ENTITY_TITLE = 0x00020016;

    //(0002,0100)
    public static final int PRIVATE_INFORMATION_CREATOR_UID = 0x00020100;

    //(0002,0102)
    public static final int PRIVATE_INFORMATION = 0x00020102;
    
    public boolean notAssignable(int tag) {
        switch (tag & 0xFFFF00FF) {
        case notModifiableTags.FILE_META_INFORMATION_LENGTH:
            return true;
        case notModifiableTags.SOURCE_APPLICATION_ENTITY_TITLE:
            return true;
        case notModifiableTags.FILE_META_INFORMATION_VERSION:
        return true;
        }
        return false;
    }
    
    public boolean notDeletable(int tag) {
        switch (tag & 0xFFFF00FF) {
        case notModifiableTags.FILE_META_INFORMATION_LENGTH:
            return true;
        case notModifiableTags.FILE_META_INFORMATION_VERSION:
            return true;
        case notModifiableTags.MEDIA_STORAGE_SOP_CLASS_UID:
            return true;
        case notModifiableTags.MEDIA_STORAGE_SOP_INSTANCE_UID:
            return true;
        case notModifiableTags.TRANSFER_SYNTAX_UID:
            return true;
        case notModifiableTags.IMPLEMENTATION_CLASS_UID:
            return true;
        case notModifiableTags.IMPLEMENTATION_VERSION_NAME:
            return true;
        case notModifiableTags.SOURCE_APPLICATION_ENTITY_TITLE:
            return true;
        case notModifiableTags.PRIVATE_INFORMATION_CREATOR_UID:
            return true;
        case notModifiableTags.PRIVATE_INFORMATION:
            return true;
        }
        return false;
    }
}
