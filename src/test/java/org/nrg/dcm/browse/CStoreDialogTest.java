/*
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.browse;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.prefs.Preferences;

import static org.mockito.Mockito.*;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class CStoreDialogTest {
    private static final String            SERVICE_1      = "AU#my.host:8104:AP";
    private static final String            SERVICE_2      = "BU#my.other.host:104:BP";
    private static final String            SERVICE_BROKEN = "bzzt!";
    private              DicomBrowser      browser;
    private              FileSetTableModel model;
    private              JFrame            frame;
    private              Preferences       prefs;

    @Before
    public void setUp() {
        InterfaceCounter.getInstance().register(this);
        browser = mock(DicomBrowser.class);
        model = mock(FileSetTableModel.class);
        frame = new JFrame();
        prefs = mock(Preferences.class);
    }

    @After
    public void tearDown() {
        browser = null;
        model = null;
        frame = null;
        prefs = null;
        InterfaceCounter.getInstance().unregister(this, false);
    }

    @Test
    @Ignore("This test works when you set a breakpoint and step into the calls in the verify() lines, indicating failure is due to issue in how mocks are being done.")
    public void testCStoreDialogSimpleHistory() {
        when(browser.getFrame()).thenReturn(frame);
        when(browser.getPrefs()).thenReturn(prefs);
        when(browser.isShowing()).thenReturn(false);    // for setLocationRelativeTo()
        when(prefs.get(matches("AE-history"), anyString())).thenReturn(SERVICE_1);

        final CStoreDialog dialog = new CStoreDialog(browser, model);
        dialog.actionPerformed(new ActionEvent(this, 0, "Send"));

        verify(model).send(eq("my.host"), eq("8104"), eq("AP"), eq("AU"), eq(false), eq(true));
        verify(prefs).put(eq("AE-history"), eq(SERVICE_1));
    }

    @Test
    @Ignore("This test works when you set a breakpoint and step into the calls in the verify() lines, indicating failure is due to issue in how mocks are being done.")
    public void testCStoreDialogCompoundHistory() {
        when(browser.getFrame()).thenReturn(frame);
        when(browser.getPrefs()).thenReturn(prefs);
        when(browser.isShowing()).thenReturn(false);    // for setLocationRelativeTo()
        when(prefs.get(matches("AE-history"), anyString())).thenReturn(SERVICE_1 + "," + SERVICE_2);

        final CStoreDialog dialog = new CStoreDialog(browser, model);
        dialog.actionPerformed(new ActionEvent(this, 0, "Send"));

        verify(model).send(eq("my.other.host"), eq("104"), eq("BP"), eq("BU"), eq(false), eq(true));
        verify(prefs).put(eq("AE-history"), eq(SERVICE_1 + "," + SERVICE_2));
    }

    @Test
    @Ignore("This test works when you set a breakpoint and step into the calls in the verify() lines, indicating failure is due to issue in how mocks are being done.")
    public void testCStoreDialogBrokenHistory() {
        when(browser.getFrame()).thenReturn(frame);
        when(browser.getPrefs()).thenReturn(prefs);
        when(browser.isShowing()).thenReturn(false);    // for setLocationRelativeTo()
        when(prefs.get(matches("AE-history"), anyString())).thenReturn(SERVICE_1 + "," + SERVICE_BROKEN);

        final CStoreDialog dialog = new CStoreDialog(browser, model);
        dialog.actionPerformed(new ActionEvent(this, 0, "Send"));

        verify(model).send(eq("my.host"), eq("8104"), eq("AP"), eq("AU"), eq(false), eq(true));
        verify(prefs).put(eq("AE-history"), eq(SERVICE_1));
    }
}
